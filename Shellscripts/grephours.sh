#!/bin/sh

#array=(00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23)
array=( 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 )

for i in "${array[@]}"
do
	grep -oiP "2020-07-09 $i:.*GET\K.*(organization)" /w/SITE1/Site1_logs/s1-app-112/w3svc1/u_ex200430.log | awk -v VAR="$i" '{print VAR " " $1}'
done

