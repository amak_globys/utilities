#!/bin/sh

#array=(00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23)
#array=( 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 )
array=( 00 01 02 03 04 05 06 )
#array=( 13 14 )

for i in "${array[@]}"
do
	grep -oiP "2020-07-09 $i:.*GET.*frontend.*" /cygdrive/z/SITE4/Site4_logs/s4-web-113/w3svc1/u_ex200709.log | grep -oP 'GET.*' | grep -oP '(^GET \S*)' | sed -re 's/\/[0-9]+//g' | tr '[:upper:]' '[:lower:]' | grep 'frontend' | awk -v VAR="$i" '{print VAR " " $1 " " $2}'
done

