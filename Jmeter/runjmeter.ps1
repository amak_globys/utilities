function Start-Sleep($seconds) {
    $doneDT = (Get-Date).AddSeconds($seconds)
    while($doneDT -gt (Get-Date)) {
        $secondsLeft = $doneDT.Subtract((Get-Date)).TotalSeconds
        $percent = ($seconds - $secondsLeft) / $seconds * 100
        Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining $secondsLeft -PercentComplete $percent
        [System.Threading.Thread]::Sleep(500)
    }
    Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining 0 -Completed
}
$jmeterfile = $args[0]
$jmeterlogparam = $args[1]
$scriptfile="script"

for ($i = 1; $i -le 5; $i += 1)
{
    $jmeterlog = $jmeterlogparam + "_" + $i + ".log"
	$scriptlog = $scriptfile + "_" + $i + ".log"
	#Start-Process -FilePath 'C:\Windows\System32\cmd.exe' -ArgumentList '/c', 'C:\Users\amak\Downloads\apache-jmeter-5.3\apache-jmeter-5.3\bin\jmeter.bat', "-n -t $jmeterfile -l $jmeterlog" -Wait -NoNewWindow -RedirectStandardOutput script.log
	Start-Process -FilePath 'C:\Windows\System32\cmd.exe' -ArgumentList '/c', 'C:\Users\qadbadmin\Desktop\apache-jmeter-5.3\bin\jmeter.bat', "-n -t $jmeterfile -l $jmeterlog" -Wait -NoNewWindow -RedirectStandardOutput $scriptlog
	Start-Sleep(300)
}



