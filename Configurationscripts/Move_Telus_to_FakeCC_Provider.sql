﻿/*
1. On your lab environment:
   ADD "oauthsrv1.corp.ad.local" to the e:\Root\Web.Config Content-Security-Policy in the frame-src section after the 'self'

2. Check the E:\GlobysModules\GlobysPaymentsBatchProcessor\GlobysPaymentsBatchProcessor.exe.config file and ensure the appSettings env value is set to your environment.

3. In Task Scheduler, Update the GlobysPaymentsBatchProcessor Actions agrument to "-c Telus -p GlobysCc -a BatchProcessor -i 60 -r 3"

4. There's a default proxy setting in the GlobysPaymentsBatchProcessor.exe.config that needed to be removed.
<add key="gasProxy" value="http://cgy1-engproxy-01.corp.ad.local:8080" />

5. RUN THE SQL SCRIPT BELOW

6.  Download the wsf file https://jira/browse/CORE-4105 and put it in e:\COM
Create a new scheduled task or import task
<?xml version="1.0" encoding="UTF-16"?>
<Task version="1.2" xmlns="http://schemas.microsoft.com/windows/2004/02/mit/task">
  <RegistrationInfo>
    <Date>2020-05-20T15:08:56.5359687</Date>
    <Author>qadbadmin</Author>
  </RegistrationInfo>
  <Triggers>
    <CalendarTrigger>
      <StartBoundary>2020-05-20T00:00:00</StartBoundary>
      <Enabled>true</Enabled>
      <ScheduleByDay>
        <DaysInterval>1</DaysInterval>
      </ScheduleByDay>
    </CalendarTrigger>
  </Triggers>
  <Principals>
    <Principal id="Author">
      <UserId>IMTELUS1083\qadbadmin</UserId>
      <LogonType>Password</LogonType>
      <RunLevel>LeastPrivilege</RunLevel>
    </Principal>
  </Principals>
  <Settings>
    <MultipleInstancesPolicy>IgnoreNew</MultipleInstancesPolicy>
    <DisallowStartIfOnBatteries>true</DisallowStartIfOnBatteries>
    <StopIfGoingOnBatteries>true</StopIfGoingOnBatteries>
    <AllowHardTerminate>true</AllowHardTerminate>
    <StartWhenAvailable>false</StartWhenAvailable>
    <RunOnlyIfNetworkAvailable>false</RunOnlyIfNetworkAvailable>
    <IdleSettings>
      <StopOnIdleEnd>true</StopOnIdleEnd>
      <RestartOnIdle>false</RestartOnIdle>
    </IdleSettings>
    <AllowStartOnDemand>true</AllowStartOnDemand>
    <Enabled>true</Enabled>
    <Hidden>false</Hidden>
    <RunOnlyIfIdle>false</RunOnlyIfIdle>
    <WakeToRun>false</WakeToRun>
    <ExecutionTimeLimit>P3D</ExecutionTimeLimit>
    <Priority>7</Priority>
  </Settings>
  <Actions Context="Author">
    <Exec>
      <Command>E:\COM\process_autopayments.wsf</Command>
    </Exec>
  </Actions>
</Task>

7.  IIS reset for good measure. (It doesn't hurt anyway)

Everything should run

To reset the machine to TELUSCc

1. Run the SQL in the commented out block below.

2. In Task Scheduler, Update the GlobysPaymentsBatchProcessor Actions agrument to "-c Telus -p TelusCc -a BatchProcessor -i 60 -r 3"

**No need to worry about updating the web.config or other settins as the new values affects nothing after the reset.
*/

INSERT t_organization_permission
VALUES (469252,6,0),
(469252,206,0);

  UPDATE t_application
  SET application_code = 'TelusCc-x'
  WHERE application_code = 'TelusCc';

 INSERT t_application([application_code],[client_id],[description],[application_category_id],[source])
 SELECT 'GlobysCc', 59,	'Client Credit Card payment processing configuration',application_category_id,'client'
 FROM t_application_category 
 WHERE category_name = 'PaymentProviders';

INSERT [dbo].[t_application_config] ([application_id],[client_id],[contents],[config_type],[config_name])
SELECT application_id,59,'{"types":["Credit"],"options":{"gatherer":"https://oauthsrv1.corp.ad.local/fakeccprovider/", "tokenizer":"/telus/frontend/payments/v1/tokenize/globys-fakecc", "height": "391px"}}','string','reactProviderConfig'
FROM t_application
WHERE application_code = 'GlobysCc';

UPDATE t_custom_value
SET char_value = 'GlobysEftCad,GlobysCc'
WHERE custom_type_id = 10004 AND default_value = 0;

IF NOT EXISTS (SELECT 1 FROM t_application_config WHERE contents = 'PaymentPostProc')
BEGIN 
  INSERT [dbo].[t_application_config] ([application_id],[client_id],[contents],[config_type],[config_name])
   SELECT application_id,59,'PaymentPostProc','string','scope'
   FROM t_application
   WHERE application_code = 'oidcStatementsFrontend';
END


/** Reset To TELUSCc Script
UPDATE t_custom_value
SET char_value = 'GlobysEftCad,TelusCc'
WHERE custom_type_id = 10004 AND default_value = 0;
  
UPDATE t_application
SET application_code = 'TelusCc'
WHERE application_code = 'TelusCc-x';

DELETE [t_application_config]
WHERE application_id IN (SELECT application_id FROM t_application WHERE application_code = 'GlobysCc');

DELETE t_application WHERE application_code = 'GlobysCc';
**/

