﻿Write-Output "Services Log GET counts"
cat e:\gas-logs\Services202007*.log | Select-String -Pattern "GET.*" |  % { $_.Matches } | % { $_.Value } | Group-Object | Sort-Object Count | select count,name  -last 20
Write-Output "********************************************************************************"
Write-Output "Frontend Log GET counts"
cat e:\gas-logs\Frontend202007*.log | Select-String -Pattern "GET.*" |  % { $_.Matches } | % { $_.Value } | Group-Object | Sort-Object Count | select count,name  -last 20
Write-Output "********************************************************************************"
Write-Output "Services Log POST counts"
cat e:\gas-logs\Services202007*.log | Select-String -Pattern "POST.*" |  % { $_.Matches } | % { $_.Value } | Group-Object | Sort-Object Count | select count,name  -last 20
Write-Output "********************************************************************************"
Write-Output "Frontend Log POST counts"
cat e:\gas-logs\Frontend202007*.log | Select-String -Pattern "POST.*" |  % { $_.Matches } | % { $_.Value } | Group-Object | Sort-Object Count | select count,name  -last 20
Write-Output "Finish reading logs $(Get-Date)"