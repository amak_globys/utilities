function Start-Sleep($seconds) {
    $doneDT = (Get-Date).AddSeconds($seconds)
    while($doneDT -gt (Get-Date)) {
        $secondsLeft = $doneDT.Subtract((Get-Date)).TotalSeconds
        $percent = ($seconds - $secondsLeft) / $seconds * 100
        Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining $secondsLeft -PercentComplete $percent
        [System.Threading.Thread]::Sleep(500)
    }
    Write-Progress -Activity "Sleeping" -Status "Sleeping..." -SecondsRemaining 0 -Completed
}

.\runjmeter.ps1 C:\users\qadbadmin\Desktop\jmeterfiles\telusapis_all900_20mwithtimeout.jmx C:\users\qadbadmin\Desktop\jmeterfiles\0725_telusapis_all900_20mwithtimeout_b315_3_11_run 315_3_11_900script
Start-Sleep(600)
.\runjmeter.ps1 C:\users\qadbadmin\Desktop\jmeterfiles\telusapis_all300_20mwithtimeout.jmx C:\users\qadbadmin\Desktop\jmeterfiles\0726_telusapis_all300_20mwithtimeout_b315_1_45_run 315_3_11_300script