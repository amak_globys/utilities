﻿
#copy upgrade.json from core build
foreach($file in Get-ChildItem -directory ) {
    Copy-Item .\upgrade.json -Destination $file\deploy
}

$currentDir = Get-Location

foreach($file in Get-ChildItem -directory ) {
    $deployDir = "$($file.FullName)\deploy"
    Set-Location $deployDir
    Write-Output "Current Directory: $(Get-Location)"
    $Readhost = Read-Host "Do you want to install $($file.FullName)\deploy\Deploy-CP.ps1 (y/n) " 
    Switch ($ReadHost) 
    { 
        Y {  
            Write-Output "Running $($file.FullName)\deploy\Deploy-CP.ps1"
            .\Deploy-CP.ps1    
        } 
        N {
            Set-Location $currentDir    
            exit
        } 
        Default { "You didn't enter the a correct response" }
    }
    Set-Location $currentDir
}



#foreach($file in Get-ChildItem -Directory) {
#    $file.CreateSubdirectory("deploy")
#}

#foreach($file in Get-ChildItem -directory ) {
#    Copy-Item .\Deploy-CP.ps1 -Destination $file\deploy
#}

#foreach($file in Get-ChildItem -directory ) {
#    Remove-Item $file\Deploy-CP*.ps1
#}


#foreach($file in Get-ChildItem -directory ) {
#    Remove-Item $file\upgrade.json
#}