#Generate jmeter report for each jmeter log file.  Will loop through all log files in directory and generate report in the report directory
#./runjmeterreport.ps1 C:\Users\amak\dev\utilities\tempjmeterlog C:\Users\amak\jmeterreport
$jmeterreportdir = $args[1]
$jmeterlogfiles = $args[0]


Get-ChildItem $jmeterlogfiles -Filter *.log |
ForEach-Object {
    $jmeterlog = $_.FullName
    $jmeterreportname = $_.BaseName
    Start-Process -FilePath 'C:\Windows\System32\cmd.exe' -ArgumentList '/c', 'C:\Users\amak\Downloads\apache-jmeter-5.3\apache-jmeter-5.3\bin\jmeter.bat', "-g $jmeterlog -o $jmeterreportdir\$jmeterreportname" -Wait -NoNewWindow
    
}
