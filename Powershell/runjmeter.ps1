$jmeterfile = $args[0]
$jmeterlogparam = "C:\Users\qadbadmin\Desktop\jmeterfiles\" + $args[1]
$scriptfile = "C:\Users\qadbadmin\Desktop\jmeterconsole\" + $args[2]
$iterations = $args[3]

for ($i = 1; $i -le $iterations; $i += 1)
{
    $currentTime = Get-Date -Format "yyyy-MM-dd HH:mm K"
    Write-Output "Starting Test run $i at $currentTime"
    $jmeterlog = $jmeterlogparam + "_" + $i + ".log"
	$scriptlog = $scriptfile + "_" + $i + ".log"
	#Start-Process -FilePath 'C:\Windows\System32\cmd.exe' -ArgumentList '/c', 'C:\Users\amak\Downloads\apache-jmeter-5.3\apache-jmeter-5.3\bin\jmeter.bat', "-n -t $jmeterfile -l $jmeterlog" -Wait -NoNewWindow -RedirectStandardOutput script.log
	Start-Process -FilePath 'C:\Windows\System32\cmd.exe' -ArgumentList '/c', 'C:\Users\qadbadmin\Desktop\apache-jmeter-5.3\bin\jmeter.bat', "-n -t $jmeterfile -l $jmeterlog" -Wait -NoNewWindow -RedirectStandardOutput $scriptlog
    $currentTime = Get-Date -Format "yyyy-MM-dd HH:mm K"
    if ($i -ne $iterations) {
        Write-Output "Sleeping for 5 minutes at $currentTime"
        Start-Sleep -s 300
    }
    else {
        $currentTime = Get-Date -Format "yyyy-MM-dd HH:mm K"
        Write-Output "Test runs finished at $currentTime"
    }
}