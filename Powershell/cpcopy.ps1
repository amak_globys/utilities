﻿$buildlocation = "\\sea1fs01\Shared\Deliverables\Internal\Builds\Clients\ATT\"
$releaselocation = "\\sea1fs01\Shared\Deliverables\External\Releases\Clients\ATT\"
$envlocationPerf = "\\sea1engfs01\Users\amak\3boxperf\"
$envlocation01 = "\\sea1engfs01\Users\amak\3boxconfignopschanges01\"
$envlocation02 = "\\sea1engfs01\Users\amak\3boxconfignopschanges02\"
$envlocation03 = "\\sea1engfs01\Users\amak\3boxconfignopschanges03\"


#To support 3 tier deployment, this function will go grab the build and copy the configuration files and rename the configuration files so that everything is ready to deploy.
#The configuration files are put in share according to which box you plan to deploy to.
#You will need a 3 tier core json for example att_e2e_core320.0.json which is located in \\sea1engfs01\Users\amak\3boxconfignopschanges01\ for the 01 box
#You will need cover_ven_vars.json from the build which you get from gitlab.com from the g3-core repo based on your build number and you put this file in for example \\sea1engfs01\Users\amak\320core_env_vars\328\core_env_vars.json
#You will need the json for the ATT CP env for example att_21.06env.json and you will get that from the build and you will put that file in for example \\sea1engfs01\Users\amak\3boxconfignopschanges01\3.5.0\237
#You will need to ensure the att_e2e_core320.0.json and att_21.06env.json have all the server names pointed to the right boxes 01, 02, or 03
#You just need to copy this powershell script to each box and run it to get the build, now you just run the appropriate deploy script for each box based on what components you need to install for CP install.
function Copy-CP ($envlocation, $release, $version, $build, $coreenvarsjsonfile, $corejson, $cpjsonfile) {
	if ($release) {
		$source = "$releaselocation$version"
		$target = "E:\ToDeploy\ATT\$version"    
	}
	else {    
		$source = "$buildlocation$version\$build"
		$target = "E:\ToDeploy\ATT\$version.$build"        
	}
	Write-Host "$source copying to $target"
	robocopy $source $target /E /mt /z
	Copy-Item $coreenvarsjsonfile -Destination "$target\Deploy"
	Copy-Item "$envlocation$corejson" -Destination "$target\Deploy\att_e2e_core.json"
	$cpjsonlocation = "$version\$build\"
	$cpjsonlocationPath = $envlocation + $cpjsonlocation
	Copy-Item "$cpjsonlocationPath\$cpjsonfile" -Destination "$target\Deploy\att_e2e-101.json"
	cd $target\Deploy
	#deploy to DB
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('SQLConfig', 'SQLAppConfig')"
	#CP3.4.3
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('SQLConfig')"
	#CP3.4.4
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('SQLConfig')"
	#CP3.5.0
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_secrets.json', '.\att_package.json', '.\att_e2e-101.json') -components ('SQLConfig', 'SQLAppConfig')"
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('SQLConfig', 'SQLAppConfig')"

	#deploy to FE
	#CP3.3.3
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('Web', 'Configure-Web')"
	#CP3.4.0
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('Web', 'Configure-Web', 'Chat-RewriteRule', 'Configure-Cat')"
	#CP3.4.1
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('Web', 'Configure-Web', 'CustomErrorPages')"
	#CP3.4.2
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('Web', 'Configure-Web', 'CustomErrorPages')"
	#CP3.4.3
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('Web', 'Configure-Web')"
	#CP3.4.4
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('Configure-Web')"
	#CP3.5.0
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_secrets.json', '.\att_package.json', '.\att_e2e-101.json') -components ('Web', 'Configure-Web', 'CustomErrorPages')"
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('Web', 'Configure-Web', 'CustomErrorPages')"

	#deploy to BE
	#CP3.4.0
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('NotificationServiceRestart')"
	#CP3.4.1
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('TLGEJB')"
	#CP3.4.3
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('TLGEJB')"
	#CP3.5.0
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_secrets.json', '.\att_package.json', '.\att_e2e-101.json') -components ('BDF', 'BdfOrderProcessor', 'TLGEJB', 'EnablerEJB', 'LargeBanPdfHandlerService', 'PdfCataloger', 'MMA')"
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('BDF', 'BdfOrderProcessor', 'TLGEJB', 'EnablerEJB', 'LargeBanPdfHandlerService', 'PdfCataloger', 'MMA')"
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('BDF', 'BdfOrderProcessor', 'TLGEJB', 'EnablerEJB', 'LargeBanPdfHandlerService', 'PdfCataloger', 'MMA', 'GlobysPaymentsBatchProcessor', 'LENMMAProcessor')"
	#powershell.exe ".\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('BDF', 'BdfOrderProcessor', 'TLGEJB', 'EnablerEJB', 'LargeBanPdfHandlerService', 'PdfCataloger', 'MMA', 'SetupAutoPaymentSetupReconciliationTask', 'SetupPaymentReconciliationTask', 'LENMMAProcessor', 'ROPSchedule'),)"
}
#Copy-CP $envlocation02 $true "3.3.1" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "att_e2e-101.json"
#Copy-CP $envlocation02 $true "3.3.2" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "att_e2e-101.json"
#Copy-CP $envlocation02 $true "3.3.7" "Release" "\\sea1engfs01\Users\amak\318.1core_env_vars\Release\core_env_vars.json" "att_e2e_core318.1.json" "att_e2e-101.json"
#Copy-CP $envlocation02 $false "3.4.0" "113" "\\sea1engfs01\Users\amak\319.1core_env_vars\28\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation02 $false "3.4.0" "129" "\\sea1engfs01\Users\amak\319.1core_env_vars\39\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocationPerf $true "3.3.2" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "att_e2e-101.json"
#Copy-CP $envlocationPerf $true "3.3.3" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "att_e2e-101.json"
#Copy-CP $envlocationPerf $true "3.3.4" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "att_e2e-101.json"
#Copy-CP $envlocationPerf $true "3.3.5" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "att_e2e-101.json"
#Copy-CP $envlocationPerf $true "3.3.6" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "att_e2e-101.json"
#Copy-CP $envlocationPerf $true "3.3.7" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "att_e2e-101.json"
#Copy-CP $envlocationPerf $false "3.4.0" "129" "\\sea1engfs01\Users\amak\319.1core_env_vars\39\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation03 $true "3.3.0" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "att_e2e-101.json"
#Copy-CP $envlocation03 $true "3.3.1" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "att_e2e-101.json"
#Copy-CP $envlocation03 $true "3.3.2" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "att_e2e-101.json"
#Copy-CP $envlocation03 $true "3.3.3" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "att_e2e-101.json"
#Copy-CP $envlocation03 $false "3.4.0" "132" "\\sea1engfs01\Users\amak\319.1core_env_vars\39\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation01 $true "3.3.7" "Release" "\\sea1engfs01\Users\amak\318.1core_env_vars\Release\core_env_vars.json" "att_e2e_core318.1.json" "att_e2e-101.json"
#Copy-CP $envlocation01 $true "3.4.0" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\51\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation02 $true "3.4.0" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation02 $true "3.4.1" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation02 $false "3.4.2" "46" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation01 $true "3.4.1" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation01 $true "3.4.2" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation01 $false "3.4.3" "8" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation01 $true "3.4.3" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation01 $true "3.4.4" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation01 $false "3.5.0" "44" "\\sea1engfs01\Users\amak\320core_env_vars\143\core_env_vars.json" "att_e2e_core320.0.json" "att_21.04env.json"
#Copy-CP $envlocation01 $false "3.5.0" "92" "\\sea1engfs01\Users\amak\320core_env_vars\174\core_env_vars.json" "att_e2e_core320.0.json" "att_21.06env.json"
#Copy-CP $envlocation02 $true "3.4.0" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation02 $true "3.4.1" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation02 $true "3.4.2" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation02 $true "3.4.3" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation02 $true "3.4.4" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "att_e2e_sample.json"
#Copy-CP $envlocation02 $false "3.5.0" "125" "\\sea1engfs01\Users\amak\320core_env_vars\207\core_env_vars.json" "att_e2e_core320.0.json" "att_21.06env.json"
#Copy-CP $envlocation01 $false "3.5.0" "159" "\\sea1engfs01\Users\amak\320core_env_vars\219\core_env_vars.json" "att_e2e_core320.0.json" "att_21.06env.json"
#Copy-CP $envlocation02 $false "3.5.0" "163" "\\sea1engfs01\Users\amak\320core_env_vars\222\core_env_vars.json" "att_e2e_core320.0.json" "att_21.06env.json"
#Copy-CP $envlocation02 $false "3.5.0" "172" "\\sea1engfs01\Users\amak\320core_env_vars\244\core_env_vars.json" "att_e2e_core320.0.json" "att_21.06env.json"
#Copy-CP $envlocation01 $false "3.5.0" "186" "\\sea1engfs01\Users\amak\320core_env_vars\257\core_env_vars.json" "att_e2e_core320.0.json" "att_21.06env.json"
#Copy-CP $envlocation02 $false "3.5.0" "220" "\\sea1engfs01\Users\amak\320core_env_vars\290\core_env_vars.json" "att_e2e_core320.0.json" "att_21.06env.json"
#Copy-CP $envlocation01 $false "3.5.0" "237" "\\sea1engfs01\Users\amak\320core_env_vars\314\core_env_vars.json" "att_e2e_core320.0.json" "att_21.06env.json"
#Copy-CP $envlocation02 $false "3.5.0" "248" "\\sea1engfs01\Users\amak\320core_env_vars\328\core_env_vars.json" "att_e2e_core320.0.json" "att_21.06env.json"
Copy-CP $envlocation01 $false "3.5.0" "283" "\\sea1engfs01\Users\amak\320core_env_vars\384\core_env_vars.json" "att_e2e_core320.0.json" "att_21.08env.json"

