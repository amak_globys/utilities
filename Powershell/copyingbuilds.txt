--report order fix
    <add key="AspDelimiter" value="×" />
    <add key="IgnoreCertErrors" value="true" />
	

--gru   305726717
--links
https://edatt20.engineering.corp.ad.local/cv/scripts/DAC0/eng/log.asp?gru=305726717
https://edatt36.engineering.corp.ad.local/CAT/Login.aspx?gru=000000000
https://edatt36.engineering.corp.ad.local/CAT/Login.aspx?gru=305726717


https://attu1.uat.globysonline.com/CAT/Login.aspx?gru=305726717 

2 Test users with permissions to create new users in CAT:

attcat1 / test1234

attcat2 / test1234

Use  “ATT” in search bar under Find Account tab to get to users and accounts


set version=3.17.0.5
set build=150
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Products\Globys\%version%\%build% E:\ToDeploy\%version%.%build% /E /mt /z

set cpversion=3.1.0
set cpbuild=190
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Clients\ATT\%cpversion%\%cpbuild% E:\ToDeploy\ATT\%cpversion%.%cpbuild% /E /mt /z

robocopy \\sea1engfs01\Users\amak\3boxconfig E:\ToDeploy\%version%.%build%\Deploy /mt /z
robocopy \\sea1engfs01\Users\amak\3boxconfig E:\ToDeploy\ATT\%cpversion%.%cpbuild%\deploy /mt /z
robocopy E:\ToDeploy\ATT\%cpversion%.%cpbuild%\deploy E:\ToDeploy\%version%.%build%\Deploy att.json att_package.json att_e2e-101.json /mt /z


--multi-box but not need for gas or common but need new version for e2e core
set version=3.17.0
set build=186
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Products\Globys\%version%\%build% E:\ToDeploy\%version%.%build% /E /mt /z

set cpversion=3.1.0
set cpbuild=219
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Clients\ATT\%cpversion%\%cpbuild% E:\ToDeploy\ATT\%cpversion%.%cpbuild% /E /mt /z

robocopy \\sea1engfs01\Users\amak\3boxconfignopschanges E:\ToDeploy\%version%.%build%\Deploy /mt /z
robocopy \\sea1engfs01\Users\amak\3boxconfignopschanges E:\ToDeploy\ATT\%cpversion%.%cpbuild%\deploy /mt /z
robocopy E:\ToDeploy\ATT\%cpversion%.%cpbuild%\deploy E:\ToDeploy\%version%.%build%\Deploy att.json att_package.json /mt /z


--multi-box but not need for gas or common but need new version for e2e core or cp since 193 is already installed for 2nd 3 box
set version=3.17.0.6
set build=170
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Products\Globys\%version%\%build% E:\ToDeploy\%version%.%build% /E /mt /z

set cpversion=3.1.0
set cpbuild=193

robocopy \\sea1engfs01\Users\amak\3boxconfignopschanges E:\ToDeploy\%version%.%build%\Deploy /mt /z
robocopy E:\ToDeploy\ATT\%cpversion%.%cpbuild%\deploy E:\ToDeploy\%version%.%build%\Deploy att.json att_package.json /mt /z



For core all boxes
.\Deploy.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_e2e-101.json')

FE deploy CP
.\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('Web', 'Web-DomainList', 'Configure-Cat', 'SSO-Frontend', 'SSO-Services')

DB deploy CP
.\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('SQLConfig', 'SQLAppConfig')

DB deploy core again after cp
.\Deploy.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_e2e-101.json') -components ('SQL')


11/24/20
update deployment scripts to use core_env_vars.json as well
For core all boxes
.\Deploy.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '\core_evn_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_e2e-101.json')

FE deploy CP
.\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '\core_evn_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('Web', 'Web-DomainList', 'Configure-Cat', 'SSO-Frontend', 'SSO-Services')

DB deploy CP
.\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '\core_evn_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('SQLConfig', 'SQLAppConfig')

DB deploy core again after cp
.\Deploy.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '\core_evn_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_e2e-101.json') -components ('SQL')




-----------single box
set version=3.17.0
set build=186
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Products\Globys\%version%\%build% E:\ToDeploy\%version%.%build% /E /mt /z
robocopy \\sea1engfs01\Users\amak\317core_env_vars\186 E:\ToDeploy\%version%.%build%\Deploy core_env_vars.json /mt /z

set cpversion=3.1.0
set cpbuild=226
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Clients\ATT\%cpversion%\%cpbuild% E:\ToDeploy\ATT\%cpversion%.%cpbuild% /E /mt /z
robocopy \\sea1engfs01\Users\amak\317core_env_vars\186 E:\ToDeploy\ATT\%cpversion%.%cpbuild%\deploy core_env_vars.json /mt /z

set version=3.18.0
set build=43
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Products\Globys\%version%\%build% E:\ToDeploy\%version%.%build% /E /mt /z
robocopy \\sea1engfs01\Users\amak\318core_env_vars\39 E:\ToDeploy\%version%.%build%\Deploy core_env_vars.json /mt /z


set cpversion=3.2.0
set cpbuild=221
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Clients\ATT\%cpversion%\%cpbuild% E:\ToDeploy\ATT\%cpversion%.%cpbuild% /E /mt /z
robocopy \\sea1engfs01\Users\amak\318core_env_vars\39 E:\ToDeploy\ATT\%cpversion%.%cpbuild%\deploy core_env_vars.json /mt /z




--copy extract single box
robocopy \\sea1engfs01\Users\amak\attphase1data F:\Load\Extract /E /mt /z
robocopy \\sea1engfs01\Users\amak\attphase1data318 F:\Load\Extract /E /mt /z


-----------single box 3.18
set version=3.18.0
set build=48
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Products\Globys\%version%\%build% E:\ToDeploy\%version%.%build% /E /mt /z
robocopy \\sea1engfs01\Users\amak\318core_env_vars\48 E:\ToDeploy\%version%.%build%\Deploy core_env_vars.json /mt /z


set cpversion=3.2.0
set cpbuild=233
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Clients\ATT\%cpversion%\%cpbuild% E:\ToDeploy\ATT\%cpversion%.%cpbuild% /E /mt /z
robocopy \\sea1engfs01\Users\amak\318core_env_vars\48 E:\ToDeploy\ATT\%cpversion%.%cpbuild%\deploy core_env_vars.json /mt /z


-----------single box 3.18 RC
set version=3.18.0.1
set build=1
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Products\Globys\%version%\%build% E:\ToDeploy\%version%.%build% /E /mt /z
robocopy \\sea1engfs01\Users\amak\318core_env_vars\48 E:\ToDeploy\%version%.%build%\Deploy core_env_vars.json /mt /z

set cpversion=3.2.0
set cpbuild=233
robocopy \\sea1fs01\Shared\Deliverables\Internal\Builds\Clients\ATT\%cpversion%\%cpbuild% E:\ToDeploy\ATT\%cpversion%.%cpbuild% /E /mt /z
robocopy \\sea1engfs01\Users\amak\318core_env_vars\48 E:\ToDeploy\ATT\%cpversion%.%cpbuild%\deploy core_env_vars.json /mt /z


REM  --deploy single box
cd E:\ToDeploy\%version%.%build%
powershell.exe ".\Deploy.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json')"
cd E:\ToDeploy\ATT\%cpversion%.%cpbuild%\deploy
powershell.exe ".\Deploy-CP.ps1"
powershell.exe ".\Deploy-CP.ps1 -components ('SSIS-DB', 'SSIS-ETL')"
robocopy \\sea1engfs01\Users\amak\attphase1data318 F:\Load\Extract /E /mt /z
REM .\Deploy.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json')  -components ('SQL')



--drop all databases
USE [master]
GO

/****** Object:  Database [att_config_prod_db]    Script Date: 10/19/2020 10:06:13 PM ******/
DROP DATABASE [att_config_prod_db]
DROP DATABASE [att_custom_db]
DROP DATABASE [att_load_db]
DROP DATABASE [att_unbilled_load_db]
DROP DATABASE [att_unbilled_usage_db]
DROP DATABASE [att_usage_prod_db]
DROP DATABASE [custom_db]
DROP DATABASE [cv_system_db]
DROP DATABASE [payment_db]

GO


--do greenfield deploy of db
drop all databases
delete all sql server agent jobs
Copy the latest 3.17 builds
.\Deploy.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_e2e-101.json') -components ('SQLGreenfield') -process ('Deploy', 'Report')
.\Deploy.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_e2e-101.json') -components ('SQLEnvConfig')

Deploy sql for 3.0.0 CP
.\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('SQL')


Old version Deploy sql for 3.0.0 CP
.\Deploy-CP.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_package.json', '.\att_e2e-101.json') -components ('SQLConfig') 

--old machines
SEA1ATTE2E-FE-101
SEA1ATTE2E-FE-1
SEA1ATTE2EFE02
SEA1ATTE2E-BE-101
SEA1ATTE2E-BE-1
SEA1ATTE2EBE02
SEA1ATTE2E-DB-101
SEA1ATTE2E-DB-1
SEA1ATTE2EDB02

SEA1ATTE2E-FE-101
SEA1ATTE2E-BE-101
SEA1ATTE2E-DB-101

SEA1ATTE2EFE01
SEA1ATTE2EBE01
SEA1ATTE2EDB01

SEA1ATTE2EFE02
SEA1ATTE2EBE02
SEA1ATTE2EDB02

SELECT a.batch_id, a.batch_type_id, a.extract_ready_dt ,b.extract_file_id,b.extract_filename,b.file_type_id,b.client_id,b.file_log_date,b.extract_start_datetime,b.extract_end_datetime,b.Error_code, f.file_type_desc,e.message
FROM t_batch a join t_extract_file b on a.extract_filename = b.extract_filename
inner join t_file_type f on f.file_type_id = b.file_type_id
full join t_sql_agent_error_log e on e.extract_file_id = b.extract_file_id
where a.batch_id > 2
ORDER BY 1 DESC, 2 DESC


update cv_system_db.dbo.t_report_order_file_server
set file_server_name = 'SEA1ATTE2E-BE-101.engineering.corp.ad.local'
where report_order_file_server_id = 1


--> Register a User / Account [Type 2]:
USE att_config_prod_db

DECLARE @user_id INT, @query_action TINYINT, @res INT, @AcctNum AS nvarchar(200), @assigned_acct_num AS nvarchar(200), @username as nvarchar(200), @lastname as nvarchar(200), @product_codes as nvarchar(10) ;
	SET @AcctNum			=	'ORGMaster2';			--	Master Acct / Organization Name
	SET @assigned_acct_num	=	'287257638234-2';		
	SET @username			=	'ATT2POB';
	SET @lastname			=	'User_2POB';
	SET @product_codes		=	'POB';
	
EXEC sp_backendapi_user_get
      @username				= @username,
      @client_id 			= 289,
      @create_user 			= 1,
      @user_id 				= @user_id OUTPUT,
      @query_action 		= @query_action OUTPUT
      
EXEC @res 					= sp_backendapi_user_update
      @user_id 				= @user_id,
      @email_addr 			= 'noemail@globysQA.com',
      @email_type_id 		= 0,
      @pay_perm 			= 0,
      @payedit_perm 		= 0,
      @termsofuse 			= NULL,
      @firstname 			= 'ATT2POB',
      @lastname 			= @lastname,
      @language_id 			= 0,
      @load_email_on 		= 1,
      @currency_code 		= 'USD',
      @permission_xml 		= NULL,
      @mobile_number 		= NULL

SELECT @res AS 'Result'

UPDATE t_user 
	SET password 			= '87F326B2351886D21ACAF357D22EFB5E' --test1234
WHERE user_id 				= @user_id 

EXEC sp_backendapi_hierarchy_user_add 
      @user_id 				= @user_id, 
      @client_id 			= 289, 
      @acct_num 			= @AcctNum, 
      @assigned_acct_num	= @assigned_acct_num, 
      @user_state 			= 5, 					-- Since we are in SSO Mode now, The @user_state = 1 should be 5 for this sproc call at the bottom
      @product_codes 		= @product_codes

SELECT * FROM att_config_prod_db..t_acct WHERE num = @AcctNum
SELECT * FROM att_config_prod_db..t_user WHERE acct_id IN (SELECT acct_id FROM att_config_prod_db..t_acct WHERE num = @AcctNum) AND usertype_id = 2
SELECT * FROM att_config_prod_db..t_acctuser WHERE acct_id IN (SELECT acct_id FROM att_config_prod_db..t_acct WHERE num = @AcctNum) AND user_id = @user_id
SELECT * FROM att_config_prod_db..t_organization WHERE organization_id IN (SELECT organization_id FROM att_config_prod_db..t_acct WHERE num = @AcctNum)
-- For SSO, You need a record in this table --
SELECT * FROM att_config_prod_db..t_user_acct WHERE user_id IN (SELECT user_id FROM att_config_prod_db..t_user WHERE acct_id IN (SELECT acct_id FROM att_config_prod_db..t_acct WHERE num = @AcctNum))
GO

--> Register a User / Account [Type 2]:
USE att_config_prod_db

DECLARE @user_id INT, @query_action TINYINT, @res INT, @AcctNum AS nvarchar(200), @assigned_acct_num AS nvarchar(200), @username as nvarchar(200), @lastname as nvarchar(200), @product_codes as nvarchar(10) ;
	SET @AcctNum			=	'ORGMaster1';			--	Master Acct / Organization Name
	SET @assigned_acct_num	=	'823642668';		
	SET @username			=	'ATT1FNB';
	SET @lastname			=	'User_1FNB';
	SET @product_codes		=	'FNB';
	
EXEC sp_backendapi_user_get
      @username				= @username,
      @client_id 			= 289,
      @create_user 			= 1,
      @user_id 				= @user_id OUTPUT,
      @query_action 		= @query_action OUTPUT
      
EXEC @res 					= sp_backendapi_user_update
      @user_id 				= @user_id,
      @email_addr 			= 'noemail@globysQA.com',
      @email_type_id 		= 0,
      @pay_perm 			= 0,
      @payedit_perm 		= 0,
      @termsofuse 			= NULL,
      @firstname 			= 'ATT1FNB',
      @lastname 			= @lastname,
      @language_id 			= 0,
      @load_email_on 		= 1,
      @currency_code 		= 'USD',
      @permission_xml 		= NULL,
      @mobile_number 		= NULL

SELECT @res AS 'Result'

UPDATE t_user 
	SET password 			= '87F326B2351886D21ACAF357D22EFB5E' --test1234
WHERE user_id 				= @user_id 

EXEC sp_backendapi_hierarchy_user_add 
      @user_id 				= @user_id, 
      @client_id 			= 289, 
      @acct_num 			= @AcctNum, 
      @assigned_acct_num	= @assigned_acct_num, 
      @user_state 			= 5, 					-- Since we are in SSO Mode now, The @user_state = 1 should be 5 for this sproc call at the bottom
      @product_codes 		= @product_codes

SELECT * FROM att_config_prod_db..t_acct WHERE num = @AcctNum
SELECT * FROM att_config_prod_db..t_user WHERE acct_id IN (SELECT acct_id FROM att_config_prod_db..t_acct WHERE num = @AcctNum) AND usertype_id = 2
SELECT * FROM att_config_prod_db..t_acctuser WHERE acct_id IN (SELECT acct_id FROM att_config_prod_db..t_acct WHERE num = @AcctNum) AND user_id = @user_id
SELECT * FROM att_config_prod_db..t_organization WHERE organization_id IN (SELECT organization_id FROM att_config_prod_db..t_acct WHERE num = @AcctNum)
-- For SSO, You need a record in this table --
SELECT * FROM att_config_prod_db..t_user_acct WHERE user_id IN (SELECT user_id FROM att_config_prod_db..t_user WHERE acct_id IN (SELECT acct_id FROM att_config_prod_db..t_acct WHERE num = @AcctNum))
GO



--edatt db
select * from cv_system_db.dbo.t_upgrade_history order by event_timestamp desc
select * from payment_db.dbo.t_upgrade_history order by event_timestamp desc
select * from att_config_prod_db.dbo.t_upgrade_history order by event_timestamp desc
select * from att_load_db.dbo.t_upgrade_history order by event_timestamp desc
select * from att_usage_prod_db.dbo.t_upgrade_history order by event_timestamp desc
select * from att_unbilled_load_db.dbo.t_upgrade_history order by event_timestamp desc

update cv_system_db.dbo.t_report_order_file_server set file_server_name = 'SEA1ATTE2EBE02.engineering.corp.ad.local'
update t_application_config set contents='https://test-premier.firstnet.att.com/cv/logproc.asp' where config_name='billAnalyst:logproc'
update t_application_config set contents='http://SEA1ATTE2EFE02.engineering.corp.ad.local'  where config_name='serviceManagement:url'
update cv_system_db.dbo.t_ce_csr_client_product_url set client_product_url = 'https://attu1.uat.globysonline.com'

--to deploy gas fix
.\Deploy.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_e2e-101.json') -components ('GAS-Frontend', 'GAS-Services')

updating to external dns
client_product_url
https://SEA1ATTE2EFE02.engineering.corp.ad.local/
select * from cv_system_db.dbo.t_ce_csr_client_product_url
client_product_url_id	client_id	consumer_flg	client_product_url	active_flg
1	289	0	https://attu1.uat.globysonline.com	1
update cv_system_db.dbo.t_ce_csr_client_product_url set client_product_url='https://attu1.uat.globysonline.com' where client_product_url_id =1

Root\web.config:80:                                     <add key="attu1.uat.globysonline.com" value="allow" />
Root\web.config:134: <add name="Content-Security-Policy" value="default-src 'self' data:; script-src 'self' data:
'unsafe-inline' 'unsafe-eval' *.googletagmanager.com *.google-analytics.com cdn.business.att.digital *.att.com;
style-src 'self' 'unsafe-inline' blob:; img-src 'self' *.google-analytics.com *.att.com *.googletagmanager.com;
frame-src 'self' *.att.com attu1.uat.globysonline.com SEA1ATTE2EFE02.engineering.corp.ad.local ; font-src 'self'
*.att.com attu1.uat.globysonline.com " />
Root\web.config:135:<add name="Access-Control-Allow-Origin" value="https://attu1.uat.globysonline.com" />

select * from t_application_config where contents like '%http%'
application_config_id	application_id	client_id	contents	config_type	config_name	last_modify_dt
42	9	289	https://localhost/cv/scripts/DAC0/eng/getsession.asp	string	billAnalyst:sessionApi	2020-10-20 09:24:35.990
43	9	289	https://localhost/cv	string	billAnalyst:internalUri	2020-10-20 09:24:35.990
44	9	289	https://test-premier.firstnet.att.com/cv/logproc.asp	string	billAnalyst:logproc	2020-10-20 09:24:35.990
46	9	289	https://SEA1ATTE2EFE02.engineering.corp.ad.local/cat	string	adminTool:logonPageUri	2020-10-20 09:24:35.990
47	10	289	http://SEA1ATTE2EFE02.engineering.corp.ad.local	url	serviceManagement:url	2020-10-20 09:24:35.990
50	10	289	<env:Envelope xmlns:env="http://www.w3.org/2003/05/soap-envelope"><env:Header><credentials><login>##username##</login><password>##password##</password></credentials></env:Header><env:Body>##body##</env:Body></env:Envelope>	string	serviceManagement:SoapEnvelope	2020-10-20 09:24:35.990
51	11	289	https://att-globys.stage.blogin.att.com/isam/sps/ATT-GLOBYSTEST/saml20	string	saml:idp	2020-10-29 20:24:37.990
52	11	289	https://test-premier.firstnet.att.com/att/frontend/auth	string	externalUri	2020-10-29 20:24:37.990
53	11	289	https://oidc.stage.flogin.att.com/mga/sps/oauth/oauth20/authorize	string	oauth:authorization	2020-10-29 20:24:37.990
57	11	289	https://oidc.stage.flogin.att.com/mga/sps/oauth/oauth20/authorize	string	oauth:token	2020-10-29 20:24:37.990
59	12	289	https://premierprofileservice-pqce17.test.att.com/restservices/premierprofile/v1/profile/inquireuser	string	att:premierUserEndpoint	2020-10-29 20:24:37.990



