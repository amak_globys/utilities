﻿$results = Get-ChildItem -Filter *.sql -Name | Select-String -Pattern '(t_.*)\.sql' | %{$_.Matches.Groups[1].value}
foreach ($result in $results) {
    Write-Host $result
    Invoke-DbaQuery -SqlInstance edatt30.engineering.corp.ad.local -Database cv_system_db -Query "select * from $result order by 1" | Export-Csv -Encoding UTF8 -Path "edatt30_$result.csv" -NoTypeInformation
    Invoke-DbaQuery -SqlInstance edtele50.engineering.corp.ad.local -Database cv_system_db -Query "select * from $result order by 1" | Export-Csv -Encoding UTF8 -Path "edtele50_$result.csv" -NoTypeInformation
    diff (cat edatt30_$result.csv) (cat edtele50_$result.csv) > edtele50_$result.csv.diff
}



#$results = Get-ChildItem -Filter *.sql -Name | Select-String -Pattern '(t_.*)\.sql' | %{$_.Matches.Groups[1].value}
#foreach ($result in $results) {
#    Write-Host $result
#    Get-ChildItem -Filter populate_$result.sql | Select-String -Pattern '^   \((.*)\)' | %{$_.Matches.Groups[1].value} | Out-File -FilePath "$result.txt"    
#}


#$results = Get-ChildItem -Filter *.sql -Name | Select-String -Pattern '(t_.*)\.sql' | %{$_.Matches.Groups[1].value}
#foreach ($result in $results) {
#(Get-Content "$result.txt").Replace("'", '') | Set-Content "$result.txt"
#}

#$result = "t_processing_result"
#Invoke-DbaQuery -SqlInstance edatt30.engineering.corp.ad.local -Database att_config_prod_db -Query "select * from $result order by 1" | Export-Csv -Encoding UTF8 -Path "edatt30_$result.csv" -NoTypeInformation
#Invoke-DbaQuery -SqlInstance edtele50.engineering.corp.ad.local -Database tele_config_prod_db -Query "select * from $result order by 1" | Export-Csv -Encoding UTF8 -Path "edtele50_$result.csv" -NoTypeInformation
#diff (cat edatt30_$result.csv) (cat edtele50_$result.csv) > edtele50_$result.csv.diff