Param(
    [string] $ClientName = "Tele",
    [string] $DataSet = "default",
    [string] $DataPath = "\\sea1engfs01\Common\Projects\TestData\NonProduction"
)

function Export-DataLoad {
  param(
      [Parameter(Mandatory=$true)]$extractFolder,
      $loadDirectory = "F:\Load\Extract"
  )
  Write-Host "Executing robocopy $extractFolder $loadDirectory"
  robocopy $extractFolder $loadDirectory
  Write-Host "Done"
  return $true
}

function UpdateUsers
{ 
    param([string]$dbName)
    $sql = 'update [cv_system_db].[dbo].[t_ce_csr_internal_user] set login_locked_date = NULL where ce_csr_internal_user_id = (select ce_csr_internal_user_id from cv_system_db.dbo.t_ce_csr_internal_user)
            update [cv_system_db].[dbo].[t_ce_csr_internal_user] set login_attempts = NULL where ce_csr_internal_user_id = (select ce_csr_internal_user_id from cv_system_db.dbo.t_ce_csr_internal_user)
            update [cv_system_db].[dbo].[t_ce_csr_internal_user] set last_login = DATEADD(DAY, -7, GETDATE()) where ce_csr_internal_user_id = (select ce_csr_internal_user_id from cv_system_db.dbo.t_ce_csr_internal_user)
            update [cv_system_db].[dbo].t_ce_csr_internal_password_history set create_date = DATEADD(DAY, -7, GETDATE()) where user_id = (select ce_csr_internal_user_id from cv_system_db.dbo.t_ce_csr_internal_user)'
   Execute-SQL -Query $sql -dbName $dbName
   Write-Host("Update user script completed")
}
function Execute-SQL
{
    param([string] $Query,
          [string] $dbName)
    $result = Invoke-Sqlcmd -Database $dbName -Query "$Query"
	Write-Host("Query to check t-batch is $query")
	Write-Host("Inside Execute-Sql: Result from sql execution is $result")
    return $result

}
function Query-SQLJobStatus
{
    param([string]$dbName,
          [string]$query,
          [int] $ExpectedEntries,
          [scriptblock]$CompleteCondition,
          [switch] $NoComplete = $false)
    $count = 0;
    Write-Host "Looking for entry in SQL table"
    write-Host "Expecting $ExpectedEntries new entries"
    $result = ""
    $current = 0
    $found  = 0
    while($count -lt 666)
    {
        Start-Sleep -Seconds 4
        $result = Execute-SQL -dbName $dbName -Query $query
		if($result -ne $null -and $result.Length -ne $null)
        {
          $found = $result.Length
        }
        if($current -ne $found)
        {
            Write-Host ("Found $found out of $ExpectedEntries")
            $current = $found
        }
        elseif($found -eq 0 -and $count -gt 80)
        {
            Write-Host "Failed to see any jobs after 5 minutes"
            return 0
        }
        if(&$CompleteCondition $result $ExpectedEntries)
        {
            Write-Host "Job completed"
            return $result
        }
        $count += 1
    }
    Write-Host "Failed to see completed job(s)"
    return 0
}

############################### Deploy DataLoad ####################################
$dbClient = "$ENV:COMPUTERNAME"
$loadDirectory = "F:\Load\Extract"
$clientPath = Join-Path $Datapath -ChildPath $ClientName | Join-Path -ChildPath $DataSet 
if ($dbClient -like "ED*" -or $dbClient -like "TF*") {
    Import-Module .\DataLoadModule.psm1 -Force
    $dbLoadName = "$($ClientName)_load_db"
    if ($ClientName -like "*WLN*") {
        $dbLoadName = "$($ClientName)_load_db1"
    }
    $BatchCompleteCheck = {
                param($result, $expect)
                if($result -and $result.Length -ge $expect)
                {if(($result | where {$_.Contains("NULL")}).Count -eq 0){return $true}}return $false
                }
	Write-Host("Clearing Data Load for Client first...")
    Clear-DataLoad
    Write-Host("Copying extract data from $clientPath to client $dbClient at $loadDirectory")
	if(Export-DataLoad -extractFolder $clientPath)
    {		
		Write-Host("Extracting Zipfiles")
		Extract-Zipfiles
        $archives = Get-ChildItem -Path $loadDirectory -r | ? {!$_.PsIsContainer -and $_.Extension -notmatch 'zip'} | Sort-Object -Unique
        Write-Host("Found $($archives.Count) files to load")
        if($archives.Count -ne 0) {
		    Write-Host("Enabling DataLoad Jobs")
            $jobName = "$($ClientName)_load_db - Find Extract Files"
            if ($ClientName -like "*WLN*") {
                $jobName = "$($ClientName)_load_db1 - Find Extract Files" 
            }
		    $startDate = Enable-DataLoadJobs -testHost $dbClient -jobName $jobName
			Write-Host("StartDate is $startDate")
            Write-Host("Waiting DataLoad completion")
            $query = "set nocount on;select completed_datetime from t_batch where created_datetime > '$startdate' ORDER BY created_datetime DESC"
            Query-SQLJobStatus -dbName $dbLoadName -query $query -ExpectedEntries $archives.Count -CompleteCondition $BatchCompleteCheck
        }
        if ($ClientName.ToLower() -like "tele") { 
			Write-Host("Tele SQL file execution started.")
			Invoke-SqlFile -clientName $ClientName -InputFile ".\withbothwhileloopsbasetelesetup.sql"			 
			Write-Host("Tele SQL file executed successfully.") 
		}
        Write-Host("Update data users")
        UpdateUsers -dbName "cv_system_db"
    }
}
else
{
    Write-Host("Dataload not supported on non ed and tf machines")
}