
#UB data load
Get-ChildItem f:\load\extract\* -Include *.dat -Recurse | Remove-Item
robocopy \\sea1fs01\Shared\Teams\Engineering\QA\ATT-Wireline\UAT\Data\RC64_DataToLoad\UB\20240919 F:\Load\ToConvert\attwln\ub /mt /z

Start-Sleep -Seconds 60

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - UB Biller - Find Files'"
Start-Sleep -Seconds 180

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - UB Biller - Process Batch'"
Start-Sleep -Seconds 300

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_load_db1 - Find Extract Files'"
Start-Sleep -Seconds 900

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - Update Account Detail Cache'"
Start-Sleep -Seconds 240

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - UB Biller - Generate Bill Log'"
Start-Sleep -Seconds 240

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - CADM - UB Post Process'"
Start-Sleep -Seconds 240

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - Data Mart - Sync Caches'"
Start-Sleep -Seconds 240


#update to UB data load
#UB data load
Get-ChildItem f:\load\extract\* -Include *.dat -Recurse | Remove-Item
robocopy \\sea1fs01\Shared\Teams\Engineering\QA\ATT-Wireline\UAT\Data\RC64_DataToLoad\UB\20240919 F:\Load\ToConvert\attwln\ub /mt /z
Start-Sleep -Seconds 30

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - UB Biller - Find Files'"
Start-Sleep -Seconds 120

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - UB Biller - Process Batch'"
Start-Sleep -Seconds 300

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_load_db1 - Find Extract Files'"
Start-Sleep -Seconds 900

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - UB Biller - Generate Bill Log'"
Start-Sleep -Seconds 240

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - Data Mart - Sync Caches'"
Start-Sleep -Seconds 240



#UB data load
#$test=Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - UB Biller - Find Files';" | Select-Object -Property last_outcome_message
#if ($test.Contains("The job succeeded" )) { 'not empty' } else { 'empty' }

#$test = Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - UB Biller - Process Batch';" | Select-Object -Property last_outcome_message
#if ($test) { 'not empty' } else { 'empty' }

#Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - UB Biller - Generate Bill Log';" | Select-Object -Property last_outcome_message
#Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - Data Mart - Sync Caches';" | Select-Object -Property last_outcome_message

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_update_job @job_name = 'attwln_load_db1 - Find Extract Files', @enabled = 0;"
Get-ChildItem f:\load\extract\* -Include *.dat -Recurse | Remove-Item
robocopy \\sea1fs01\Shared\Teams\Engineering\QA\ATT-Wireline\UAT\Data\RC65_DataToLoad\UB\20231221_ConvergedAccounts F:\Load\ToConvert\attwln\ub /mt /z
Start-Sleep -Seconds 30

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - UB Biller - Find Files'"
Write-Host("attwln_integrations - UB Biller - Find Files")
Start-Sleep -Seconds 180

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - UB Biller - Process Batch'"
Write-Host("attwln_integrations - UB Biller - Process Batch")
Start-Sleep -Seconds 480

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_update_job @job_name = 'attwln_load_db1 - Find Extract Files', @enabled = 1;"
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_load_db1 - Find Extract Files'"
Write-Host("attwln_load_db1 - Find Extract Files")
Start-Sleep -Seconds 900

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - UB Biller - Generate Bill Log'"
Write-Host("attwln_integrations - UB Biller - Generate Bill Log")
Start-Sleep -Seconds 180

Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - Data Mart - Sync Caches'"
Write-Host("attwln_integrations - Data Mart - Sync Caches")
Start-Sleep -Seconds 180



while($count -le 25) 
{
	Get-Date
	Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - Run CADM Integration'" | Select-Object -Property name, last_outcome_message, last_run_date, last_run_time 
	#Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - Update Account Detail Cache'" | Select-Object -Property name, last_outcome_message, last_run_date, last_run_time 
	#Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - CADM - UB Post Process'" | Select-Object -Property name, last_outcome_message, last_run_date, last_run_time 
	#Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - UB Biller - Find Files'" | Select-Object -Property name, last_outcome_message, last_run_date, last_run_time 
	#Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - UB Biller - Process Batch'" | Select-Object -Property name, last_outcome_message, last_run_date, last_run_time 
	#Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - Update Account Detail Cache'" | Select-Object -Property name, last_outcome_message, last_run_date, last_run_time 
	#Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - UB Biller - Generate Bill Log'" | Select-Object -Property name, last_outcome_message, last_run_date, last_run_time
	#Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - CADM - UB Post Process'" | Select-Object -Property name, last_outcome_message, last_run_date, last_run_time
	#Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = 'attwln_integrations - Data Mart - Sync Caches'" | Select-Object -Property name, last_outcome_message, last_run_date, last_run_time
	
	#Write-Host($test)
	Start-Sleep -Seconds 20
}

#--cadm and ub together
#Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_help_job @job_name = ;" | Select-Object -Property last_outcome_message
#get CADM files
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_update_job @job_name = 'attwln_load_db1 - Find Extract Files', @enabled = 0;"
Get-ChildItem f:\load\extract\* -Include *.dat -Recurse | Remove-Item
cp \\sea1fs01\Shared\Teams\Engineering\QA\ATT-Wireline\UAT\Data\RC65_DataToLoad\CADM\SDP_FUB_ID000068_T20231107013338.DAT F:\Load\ToConvert\attwln\cadm
cp \\sea1fs01\Shared\Teams\Engineering\QA\ATT-Wireline\UAT\Data\RC65_DataToLoad\CADM\SDP_FUB_ID000068_T20240110063801.DAT F:\Load\ToConvert\attwln\cadm
#cp \\sea1fs01\Shared\Teams\Engineering\QA\ATT-Wireline\UAT\Data\RC65_DataToLoad\CADM\SDP_FUB_ID000084_T20240828040532.DAT F:\Load\ToConvert\attwln\cadm



Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - Run CADM Integration'"

#run ETL
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_update_job @job_name = 'attwln_load_db1 - Find Extract Files', @enabled = 1;"
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_load_db1 - Find Extract Files'"

#after ETL
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - Update Account Detail Cache'"
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - CADM - UB Post Process'"



#get UB files
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_update_job @job_name = 'attwln_load_db1 - Find Extract Files', @enabled = 0;"
Get-ChildItem f:\load\extract\* -Include *.dat -Recurse | Remove-Item
robocopy \\sea1fs01\Shared\Teams\Engineering\QA\ATT-Wireline\UAT\Data\RC65_DataToLoad\UB\20231221_ConvergedAccounts F:\Load\ToConvert\attwln\ub /mt /z
#robocopy \\sea1fs01\Shared\Teams\Engineering\QA\ATT-Wireline\UAT\Data\RC65_DataToLoad\UB\20240821 F:\Load\ToConvert\attwln\ub /mt /z


Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - UB Biller - Find Files'"
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - UB Biller - Process Batch'"

#run ETL
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "EXEC msdb.dbo.sp_update_job @job_name = 'attwln_load_db1 - Find Extract Files', @enabled = 1;"
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_load_db1 - Find Extract Files'"

#after ETL
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - Update Account Detail Cache'"
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - UB Biller - Generate Bill Log'"
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - CADM - UB Post Process'"
Invoke-Sqlcmd -ServerInstance $ENV:COMPUTERNAME -Query "use msdb; exec sp_start_job @job_name = 'attwln_integrations - Data Mart - Sync Caches'"

