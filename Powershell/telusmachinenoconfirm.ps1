﻿
#copy upgrade.json from core build
foreach($file in Get-ChildItem -directory ) {
    Copy-Item .\upgrade.json -Destination $file\deploy
}

$currentDir = Get-Location

foreach($file in Get-ChildItem -directory ) {
    $deployDir = "$($file.FullName)\deploy"
    Set-Location $deployDir
    Write-Output "Current Directory: $(Get-Location)"
    Write-Output "Running $($file.FullName)\deploy\Deploy-CP.ps1"
	try {
    .\Deploy-CP.ps1    
	}
	catch {
		Write-Output "Failed Directory: $(Get-Location)"
	}
}

#foreach($file in Get-ChildItem -Directory) {
#    $file.CreateSubdirectory("deploy")
#}

#foreach($file in Get-ChildItem -directory ) {
#    Remove-Item $file\Deploy-CP*.ps1
#}