USE tele_config_prod_db

/*
DECLARE 
@running INT = 1,
@seccount INT = 0,
@maxseccount INT = 600

 WHILE @running<>0 AND @seccount < @maxseccount
    BEGIN
        WAITFOR DELAY '0:0:05';
        set @seccount = @seccount + 5
        IF NOT (SELECT TOP 1 completed_datetime FROM tele_load_db.dbo.t_batch ORDER BY batch_id DESC) IS NOT NULL
            BEGIN 
                SET @running = 0
            END
    END

IF @seccount >= @maxseccount
	SET @seccount = 501
ELSE
	SET @seccount = 0
SET @running = 1
SET @maxseccount = 500
 WHILE @running<>0 AND @seccount < @maxseccount
    BEGIN
        WAITFOR DELAY '0:0:05';
        set @seccount = @seccount + 5
        IF (SELECT TOP 1 completed_datetime FROM tele_load_db.dbo.t_batch ORDER BY batch_id DESC) IS NOT NULL
            BEGIN 
                SET @running = 0
            END
    END
	
*/
--dashboard module setup
use tele_config_prod_db

/** Script Created 02/18/2022 Based On CORE Version 3.22.0.130 and Client Version Tele_3.0.0.247 **/
     DECLARE @newReportId INT,
     @DeleteFlg TINYINT = 0,
     @dupeRptID INT,
     @calcId INT = 0,
     @groupFieldId INT = 0,
     @default_callgroup_id INT,
     @module_id  INT

     SELECT @module_id = module_id FROM t_dashboard_2_module WHERE module_name = '{"eng":"Account Spend"}'

     IF ISNULL(@module_id,0)>0
     BEGIN   
       DELETE t_dashboard_2_layout WHERE module_id = @module_id
       DELETE t_dashboard_2_module WHERE module_id = @module_id
     END

     SELECT @dupeRptID = summary_id FROM t_summary (NOLOCK) WHERE report_type_id = 34  AND nme = 'Dashboard' AND user_id = 0
     IF ISNULL(@dupeRptID,0)>0
     BEGIN   
       DELETE t_summary WHERE summary_id = @dupeRptID
       SET @DeleteFlg = 1
     END
     SELECT @calcId = calculation_id FROM t_calculation WHERE column_nme = 'numeric_1' and report_type_id = 34 

     
      
     SELECT @groupFieldId = field_id FROM t_field WHERE nme = 'Account Number' and report_type_id = 34 

     INSERT t_summary (user_id, rateplan_id, nme, grp_field_id, grpsortby_flg, grpsorttype_flg, grpminval_flg, grpminval_qty, grpother_flg, subgrp_field_id, subgrpsortby_flg, subgrpsorttype_flg, subgrpminval_flg, subgrpminval_qty, subgrpother_flg, totalcalls_flg, totallength_flg, totalcost_flg, costpercall_flg, costperminute_flg, averagecalllength_flg, calccosttype_flg, rateplanoverride_flg, totalitems_flg, totalunits_flg, totalsubunits_flg, costperitem_flg, costperunit_flg, costpersubunit_flg, default_view, graph_type, graph_calc_type, graph_none_flg, graph_belowmin_flg, product_id, report_type_id, shared_flg, default_callgroup_id, group_id, graph_barwidth, graph_barseparation, graph_linetype, graph_rotation_x, graph_rotation_y, graph_palette, graph_legend, graph_values, graph_labels, graph_border, graph_grid, graph_3d, rpt_run_count, last_run_date, subsubgrp_field_id, subsubgrpsortby_flg, subsubgrpsorttype_flg, subsubgrpminval_flg, subsubgrpminval_qty, subsubgrpother_flg, subsubsubgrp_field_id, subsubsubgrpsortby_flg, subsubsubgrpsorttype_flg, subsubsubgrpminval_flg, subsubsubgrpminval_qty, subsubsubgrpother_flg, group_total_location, suppress_subtotals)
     VALUES(0,0,'Dashboard', ISNULL(@groupFieldId,0),93,1,0,0.0000,1,0,0,0,0,0.0000,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1, ISNULL(@calcId,0),1,0,null,34,1, @default_callgroup_id,null,0,0,0,0,0,0,0,1,1,0,0,0,0,null,0,0,0,0,0.0000,1,0,0,0,0,0.0000,1,0,0);

     SELECT @newReportID = @@IDENTITY
     INSERT INTO t_summary_calculation (summary_id, calculation_id, order_seq) 
     SELECT @newReportID,c.calculation_id, 1
     FROM t_calculation c
     WHERE report_type_id = 34
     AND c.nme like '%eng&&Incl GST $%'

     IF @DeleteFlg = 1
     BEGIN
       DELETE t_summary_calculation WHERE summary_id = @dupeRptID;
     END
      
  DECLARE @new_module_id INT 
  INSERT t_dashboard_2_module (module_name, configuration, module_type_id, title_language_translation_type_id)
  VALUES ('{"eng":"Account Spend"}', '<config><param name="FolderLang" translationId="0"/><content><![CDATA[
  <div class="panel panel-default panel-report-module" data-module-id="2010" >
  <div id="panel-heading-2010" class="panel-heading panel-heading-admin"> <span id="panel-title-2010" class="module-title panel-title"></span> </div>
  <div id="panel-body-2010" class="panel-body" style="position: relative;">
  </div>
  <div id="panel-footer-2010" class="panel-module-footer"></div>
</div>
<script>
jQuery(document).ready(function() {
var template_function = hbar;
var moduleConfig = {
  reportId: 6050 ,
  reportType: "Summary",
  baseColor: "rgb(51,122,183)",
  bmType: "c",
  startDate:"",
  rowCount: 10,
  moduleTitle: {"eng":"Account Spend"},
  panelBody: "panel-body-2010",
  panelTitle: "panel-title-2010",
  panelFooter: "panel-footer-2010",
  legendOptions:"None",
  height:"350",
  usePalette:"off",
  colorPalette:""
}; // moduleConfig_end - keep comment for upgrade
jQuery("#panel-body-2010").text(window.strings["TABLE_LOADING"]);
if(moduleConfig.bmType == "c"){
  var d = new Date();
  var yr = d.getFullYear();
  var mo = d.getMonth()+1;
  var startDate = yr.toString() + "-" + mo.toString() + "-01";
  moduleConfig.startDate = startDate;}
var panel_function = new template_function (moduleConfig);
jQuery( "body" ).on( "localizationLoad", function() {
  if(moduleConfig.bmType != "c"){
    panel_function.draw();
  }
});
jQuery("#month-select-date-control").on( "change", function() {
  if(moduleConfig.bmType == "c"){
    jQuery("#panel-body-2010").text(window.strings["TABLE_LOADING"]);
    var startDate = document.getElementById("month-select-date-control").value + "-01";
    moduleConfig.startDate = startDate;
    panel_function.draw();
    }
  });
});
</script>
 ]]></content></config>',0,1111)

 
    SELECT @new_module_id = SCOPE_IDENTITY() 

    UPDATE t_dashboard_2_module
    SET  configuration = REPLACE(REPLACE(Replace(configuration,'"2010"' ,'"'+CONVERT(NVARCHAR,@new_module_id)+'"'),'-2010"','-'+CONVERT(NVARCHAR,@new_module_id)+'"'),'reportId: 6050 ,','reportId: '+CONVERT(NVARCHAR,@newReportID)+' ,') 
    WHERE module_id = CONVERT(NVARCHAR, @new_module_id)

    IF @DeleteFlg = 1
    BEGIN
      UPDATE t_dashboard_2_module
      SET configuration = Replace(configuration,'reportId: ' + CONVERT(NVARCHAR,@dupeRptID)+ ' ,','reportId: '+CONVERT(NVARCHAR,@newReportID)+' ,') 
    END

    INSERT [dbo].[t_dashboard_2_layout] (layout_id, module_id, display_column, display_row, state)
    VALUES (0, CONVERT(NVARCHAR, @new_module_id),1,2,0);   
     WITH RowNum_Reset AS (
     SELECT[layout_id],
     module_id,
     display_column,
     display_row,
     ROW_NUMBER() OVER (PARTITION BY LAYOUT_ID, DISPLAY_COLUMN ORDER BY display_row, module_id DESC ) as New_Row
     FROM t_dashboard_2_layout (NOLOCK)
   )
   UPDATE RowNum_Reset
   SET display_row = New_Row;
   GO 
   
 BEGIN 
  DECLARE @custom_value_id INT; 
  DECLARE @color_palette NVARCHAR(MAX) = 'rgb(0,96,210),rgb(58,174,231),rgb(75,201,194),rgb(145,220,44),rgb(128,255,128),rgb(255,255,0),rgb(251,173,68),rgb(236,75,21),rgb(255,0,0),rgb(236,19,143),rgb(184,14,180),rgb(145,19,155),rgb(117,6,153),rgb(22,85,148),rgb(151,151,255),rgb(70,202,217),rgb(136,226,35),rgb(180,46,22),rgb(255,177,100),rgb(255,148,255)'; 
  SELECT @custom_value_id = custom_value_id FROM t_client_custom (NOLOCK)WHERE client_id = 666 AND custom_type_id = 4212;
  IF @custom_value_id IS NOT NULL 
   BEGIN 
    UPDATE t_custom_value SET char_value = @color_palette WHERE custom_value_id = @custom_value_id AND custom_type_id = 4212; 
   END 
  ELSE 
   BEGIN 
    DELETE t_client_custom where custom_type_id = 4212; 
    DELETE t_custom_value where custom_type_id = 4212 and default_value = 0; 
    INSERT[dbo].[t_custom_value](custom_type_id, description, char_value, default_value) 
    SELECT 4212, 'Custom Color Palette for Client', @color_palette, 0; 
    SELECT @custom_value_id = custom_value_id 
    FROM t_custom_value 
    WHERE custom_type_id = 4212 
    AND default_value = 0; 
    INSERT t_client_custom (client_id, custom_value_id, custom_type_id) 
    SELECT 666, @custom_value_id, 4212; 
   END 
END 
GO

--dashboard 1 (another widget)
use tele_config_prod_db
/** Script Created 03/14/2022 Based On CORE Version 3.22.1.41 and Client Version Tele_3.0.0.252 **/
     DECLARE @newReportId INT,
     @DeleteFlg TINYINT = 0,
     @dupeRptID INT,
     @calcId INT = 0,
     @groupFieldId INT = 0,
     @default_callgroup_id INT,
     @module_id  INT

     SELECT @module_id = module_id FROM t_dashboard_2_module WHERE module_name = '{"eng":"Dashboard 2"}'

     IF ISNULL(@module_id,0)>0
     BEGIN   
       DELETE t_dashboard_2_layout WHERE module_id = @module_id
       DELETE t_dashboard_2_module WHERE module_id = @module_id
     END

     SELECT @dupeRptID = summary_id FROM t_summary (NOLOCK) WHERE report_type_id = 17  AND nme = 'Dashboard 2' AND user_id = 0
     IF ISNULL(@dupeRptID,0)>0
     BEGIN   
       DELETE t_summary WHERE summary_id = @dupeRptID
       SET @DeleteFlg = 1
     END
     SELECT @calcId = calculation_id FROM t_calculation WHERE column_nme = 'totalitems' and report_type_id = 17 

     
      
     SELECT @groupFieldId = field_id FROM t_field WHERE nme = 'Service Number' and report_type_id = 17 

     INSERT t_summary (user_id, rateplan_id, nme, grp_field_id, grpsortby_flg, grpsorttype_flg, grpminval_flg, grpminval_qty, grpother_flg, subgrp_field_id, subgrpsortby_flg, subgrpsorttype_flg, subgrpminval_flg, subgrpminval_qty, subgrpother_flg, totalcalls_flg, totallength_flg, totalcost_flg, costpercall_flg, costperminute_flg, averagecalllength_flg, calccosttype_flg, rateplanoverride_flg, totalitems_flg, totalunits_flg, totalsubunits_flg, costperitem_flg, costperunit_flg, costpersubunit_flg, default_view, graph_type, graph_calc_type, graph_none_flg, graph_belowmin_flg, product_id, report_type_id, shared_flg, default_callgroup_id, group_id, graph_barwidth, graph_barseparation, graph_linetype, graph_rotation_x, graph_rotation_y, graph_palette, graph_legend, graph_values, graph_labels, graph_border, graph_grid, graph_3d, rpt_run_count, last_run_date, subsubgrp_field_id, subsubgrpsortby_flg, subsubgrpsorttype_flg, subsubgrpminval_flg, subsubgrpminval_qty, subsubgrpother_flg, subsubsubgrp_field_id, subsubsubgrpsortby_flg, subsubsubgrpsorttype_flg, subsubsubgrpminval_flg, subsubsubgrpminval_qty, subsubsubgrpother_flg, group_total_location, suppress_subtotals)
     VALUES(0,0,'Dashboard 2', ISNULL(@groupFieldId,0),3,1,0,0.0000,1,0,0,0,0,0.0000,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1, ISNULL(@calcId,0),1,0,null,17,1, @default_callgroup_id,null,0,0,0,0,0,0,0,1,1,0,0,0,0,null,0,0,0,0,0.0000,1,0,0,0,0,0.0000,1,0,0);

     SELECT @newReportID = @@IDENTITY
     INSERT INTO t_summary_calculation (summary_id, calculation_id, order_seq) 
     SELECT @newReportID,c.calculation_id, 1
     FROM t_calculation c
     WHERE report_type_id = 17
     AND c.nme like N'%eng&&Number Of Calls%'

     IF @DeleteFlg = 1
     BEGIN
       DELETE t_summary_calculation WHERE summary_id = @dupeRptID;
     END
      
  DECLARE @new_module_id INT 
  INSERT t_dashboard_2_module (module_name, configuration, module_type_id, title_language_translation_type_id)
  VALUES ('{"eng":"Dashboard 2"}', '<config><param name="FolderLang" translationId="0"/><content><![CDATA[
  <div class="panel panel-default panel-report-module" data-module-id="2013" >
  <div id="panel-heading-2013" class="panel-heading panel-heading-admin"> <span id="panel-title-2013" class="module-title panel-title"></span> </div>
  <div id="panel-body-2013" class="panel-body" style="position: relative;">
  </div>
  <div id="panel-footer-2013" class="panel-module-footer"></div>
</div>
<script>
jQuery(document).ready(function() {
var template_function = doughnut;
var moduleConfig = {
  reportId: 6051 ,
  reportType: "Summary",
  baseColor: "rgb(51,122,183)",
  bmType: "c",
  startDate:"",
  rowCount: 10,
  moduleTitle: {"eng":"Dashboard 2"},
  panelBody: "panel-body-2013",
  panelTitle: "panel-title-2013",
  panelFooter: "panel-footer-2013",
  legendOptions:"None",
  height:"300",
  usePalette:"off",
  colorPalette:""
}; // moduleConfig_end - keep comment for upgrade
jQuery("#panel-body-2013").text(window.strings["TABLE_LOADING"]);
if(moduleConfig.bmType == "c"){
  var d = new Date();
  var yr = d.getFullYear();
  var mo = d.getMonth()+1;
  var startDate = yr.toString() + "-" + mo.toString() + "-01";
  moduleConfig.startDate = startDate;}
var panel_function = new template_function (moduleConfig);
jQuery( "body" ).on( "localizationLoad", function() {
  if(moduleConfig.bmType != "c"){
    panel_function.draw();
  }
});
jQuery("#month-select-date-control").on( "change", function() {
  if(moduleConfig.bmType == "c"){
    jQuery("#panel-body-2013").text(window.strings["TABLE_LOADING"]);
    var startDate = document.getElementById("month-select-date-control").value + "-01";
    moduleConfig.startDate = startDate;
    panel_function.draw();
    }
  });
});
</script>
 ]]></content></config>',0,1111)

 
    SELECT @new_module_id = SCOPE_IDENTITY() 

    UPDATE t_dashboard_2_module
    SET  configuration = REPLACE(REPLACE(Replace(configuration,'"2013"' ,'"'+CONVERT(NVARCHAR,@new_module_id)+'"'),'-2013"','-'+CONVERT(NVARCHAR,@new_module_id)+'"'),'reportId: 6051 ,','reportId: '+CONVERT(NVARCHAR,@newReportID)+' ,') ,
    report_id = @newReportId,
    report_type = 'Summary',
    report_type_id = 17
    WHERE module_id = CONVERT(NVARCHAR, @new_module_id)

    IF @DeleteFlg = 1
    BEGIN
      UPDATE t_dashboard_2_module
      SET configuration = Replace(configuration,'reportId: ' + CONVERT(NVARCHAR,@dupeRptID)+ ' ,','reportId: '+CONVERT(NVARCHAR,@newReportID)+' ,') 
    END

    INSERT [dbo].[t_dashboard_2_layout] (layout_id, module_id, display_column, display_row, state)
    VALUES (0, CONVERT(NVARCHAR, @new_module_id),2,2,0);   
     WITH RowNum_Reset AS (
     SELECT[layout_id],
     module_id,
     display_column,
     display_row,
     ROW_NUMBER() OVER (PARTITION BY LAYOUT_ID, DISPLAY_COLUMN ORDER BY display_row, module_id DESC ) as New_Row
     FROM t_dashboard_2_layout (NOLOCK)
   )
   UPDATE RowNum_Reset
   SET display_row = New_Row;
   GO 
   
 BEGIN 
  DECLARE @custom_value_id INT; 
  DECLARE @color_palette NVARCHAR(MAX) = 'rgb(0,96,210),rgb(58,174,231),rgb(75,201,194),rgb(145,220,44),rgb(128,255,128),rgb(255,255,0),rgb(251,173,68),rgb(236,75,21),rgb(255,0,0),rgb(236,19,143),rgb(184,14,180),rgb(145,19,155),rgb(117,6,153),rgb(22,85,148),rgb(151,151,255),rgb(70,202,217),rgb(136,226,35),rgb(180,46,22),rgb(255,177,100),rgb(255,148,255)'; 
  SELECT @custom_value_id = custom_value_id FROM t_client_custom (NOLOCK)WHERE client_id = 666 AND custom_type_id = 4212;
  IF @custom_value_id IS NOT NULL 
   BEGIN 
    UPDATE t_custom_value SET char_value = @color_palette WHERE custom_value_id = @custom_value_id AND custom_type_id = 4212; 
   END 
  ELSE 
   BEGIN 
    DELETE t_client_custom where custom_type_id = 4212; 
    DELETE t_custom_value where custom_type_id = 4212 and default_value = 0; 
    INSERT[dbo].[t_custom_value](custom_type_id, description, char_value, default_value) 
    SELECT 4212, 'Custom Color Palette for Client', @color_palette, 0; 
    SELECT @custom_value_id = custom_value_id 
    FROM t_custom_value 
    WHERE custom_type_id = 4212 
    AND default_value = 0; 
    INSERT t_client_custom (client_id, custom_value_id, custom_type_id) 
    SELECT 666, @custom_value_id, 4212; 
   END 
END 
GO



--dashboard 2 (another widget)
/** Script Created 03/14/2022 Based On CORE Version 3.22.1.41 and Client Version Tele_3.0.0.252 **/
     DECLARE @newReportId INT,
     @DeleteFlg TINYINT = 0,
     @dupeRptID INT,
     @calcId INT = 0,
     @groupFieldId INT = 0,
     @default_callgroup_id INT,
     @module_id  INT

     SELECT @module_id = module_id FROM t_dashboard_2_module WHERE module_name = '{"eng":"Dashboard 1"}'

     IF ISNULL(@module_id,0)>0
     BEGIN   
       DELETE t_dashboard_2_layout WHERE module_id = @module_id
       DELETE t_dashboard_2_module WHERE module_id = @module_id
     END

     SELECT @dupeRptID = summary_id FROM t_summary (NOLOCK) WHERE report_type_id = 17  AND nme = 'Dashboard 1' AND user_id = 0
     IF ISNULL(@dupeRptID,0)>0
     BEGIN   
       DELETE t_summary WHERE summary_id = @dupeRptID
       SET @DeleteFlg = 1
     END
     SELECT @calcId = calculation_id FROM t_calculation WHERE column_nme = 'totalitems' and report_type_id = 17 

     
      
     SELECT @groupFieldId = field_id FROM t_field WHERE nme = 'Service Number' and report_type_id = 17 

     INSERT t_summary (user_id, rateplan_id, nme, grp_field_id, grpsortby_flg, grpsorttype_flg, grpminval_flg, grpminval_qty, grpother_flg, subgrp_field_id, subgrpsortby_flg, subgrpsorttype_flg, subgrpminval_flg, subgrpminval_qty, subgrpother_flg, totalcalls_flg, totallength_flg, totalcost_flg, costpercall_flg, costperminute_flg, averagecalllength_flg, calccosttype_flg, rateplanoverride_flg, totalitems_flg, totalunits_flg, totalsubunits_flg, costperitem_flg, costperunit_flg, costpersubunit_flg, default_view, graph_type, graph_calc_type, graph_none_flg, graph_belowmin_flg, product_id, report_type_id, shared_flg, default_callgroup_id, group_id, graph_barwidth, graph_barseparation, graph_linetype, graph_rotation_x, graph_rotation_y, graph_palette, graph_legend, graph_values, graph_labels, graph_border, graph_grid, graph_3d, rpt_run_count, last_run_date, subsubgrp_field_id, subsubgrpsortby_flg, subsubgrpsorttype_flg, subsubgrpminval_flg, subsubgrpminval_qty, subsubgrpother_flg, subsubsubgrp_field_id, subsubsubgrpsortby_flg, subsubsubgrpsorttype_flg, subsubsubgrpminval_flg, subsubsubgrpminval_qty, subsubsubgrpother_flg, group_total_location, suppress_subtotals)
     VALUES(0,0,'Dashboard 1', ISNULL(@groupFieldId,0),3,1,0,0.0000,1,0,0,0,0,0.0000,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1, ISNULL(@calcId,0),1,0,null,17,1, @default_callgroup_id,null,0,0,0,0,0,0,0,1,1,0,0,0,0,null,0,0,0,0,0.0000,1,0,0,0,0,0.0000,1,0,0);

     SELECT @newReportID = @@IDENTITY
     INSERT INTO t_summary_calculation (summary_id, calculation_id, order_seq) 
     SELECT @newReportID,c.calculation_id, 1
     FROM t_calculation c
     WHERE report_type_id = 17
     AND c.nme like N'%eng&&Number Of Calls%'

     IF @DeleteFlg = 1
     BEGIN
       DELETE t_summary_calculation WHERE summary_id = @dupeRptID;
     END
      
  DECLARE @new_module_id INT 
  INSERT t_dashboard_2_module (module_name, configuration, module_type_id, title_language_translation_type_id)
  VALUES ('{"eng":"Dashboard 1"}', '<config><param name="FolderLang" translationId="0"/><content><![CDATA[
  <div class="panel panel-default panel-report-module" data-module-id="2012" >
  <div id="panel-heading-2012" class="panel-heading panel-heading-admin"> <span id="panel-title-2012" class="module-title panel-title"></span> </div>
  <div id="panel-body-2012" class="panel-body" style="position: relative;">
  </div>
  <div id="panel-footer-2012" class="panel-module-footer"></div>
</div>
<script>
jQuery(document).ready(function() {
var template_function = line;
var moduleConfig = {
  reportId: 6051 ,
  reportType: "Summary",
  baseColor: "rgb(51,122,183)",
  bmType: "c",
  startDate:"",
  rowCount: 10,
  moduleTitle: {"eng":"Dashboard 1"},
  panelBody: "panel-body-2012",
  panelTitle: "panel-title-2012",
  panelFooter: "panel-footer-2012",
  legendOptions:"None",
  height:"350",
  usePalette:"off",
  colorPalette:""
}; // moduleConfig_end - keep comment for upgrade
jQuery("#panel-body-2012").text(window.strings["TABLE_LOADING"]);
if(moduleConfig.bmType == "c"){
  var d = new Date();
  var yr = d.getFullYear();
  var mo = d.getMonth()+1;
  var startDate = yr.toString() + "-" + mo.toString() + "-01";
  moduleConfig.startDate = startDate;}
var panel_function = new template_function (moduleConfig);
jQuery( "body" ).on( "localizationLoad", function() {
  if(moduleConfig.bmType != "c"){
    panel_function.draw();
  }
});
jQuery("#month-select-date-control").on( "change", function() {
  if(moduleConfig.bmType == "c"){
    jQuery("#panel-body-2012").text(window.strings["TABLE_LOADING"]);
    var startDate = document.getElementById("month-select-date-control").value + "-01";
    moduleConfig.startDate = startDate;
    panel_function.draw();
    }
  });
});
</script>
 ]]></content></config>',0,1111)

 
    SELECT @new_module_id = SCOPE_IDENTITY() 

    UPDATE t_dashboard_2_module
    SET  configuration = REPLACE(REPLACE(Replace(configuration,'"2012"' ,'"'+CONVERT(NVARCHAR,@new_module_id)+'"'),'-2012"','-'+CONVERT(NVARCHAR,@new_module_id)+'"'),'reportId: 6051 ,','reportId: '+CONVERT(NVARCHAR,@newReportID)+' ,') ,
    report_id = @newReportId,
    report_type = 'Summary',
    report_type_id = 17
    WHERE module_id = CONVERT(NVARCHAR, @new_module_id)

    IF @DeleteFlg = 1
    BEGIN
      UPDATE t_dashboard_2_module
      SET configuration = Replace(configuration,'reportId: ' + CONVERT(NVARCHAR,@dupeRptID)+ ' ,','reportId: '+CONVERT(NVARCHAR,@newReportID)+' ,') 
    END

    INSERT [dbo].[t_dashboard_2_layout] (layout_id, module_id, display_column, display_row, state)
    VALUES (0, CONVERT(NVARCHAR, @new_module_id),1,4,0);   
     WITH RowNum_Reset AS (
     SELECT[layout_id],
     module_id,
     display_column,
     display_row,
     ROW_NUMBER() OVER (PARTITION BY LAYOUT_ID, DISPLAY_COLUMN ORDER BY display_row, module_id DESC ) as New_Row
     FROM t_dashboard_2_layout (NOLOCK)
   )
   UPDATE RowNum_Reset
   SET display_row = New_Row;
   GO 
   
 BEGIN 
  DECLARE @custom_value_id INT; 
  DECLARE @color_palette NVARCHAR(MAX) = 'rgb(0,96,210),rgb(58,174,231),rgb(75,201,194),rgb(145,220,44),rgb(128,255,128),rgb(255,255,0),rgb(251,173,68),rgb(236,75,21),rgb(255,0,0),rgb(236,19,143),rgb(184,14,180),rgb(145,19,155),rgb(117,6,153),rgb(22,85,148),rgb(151,151,255),rgb(70,202,217),rgb(136,226,35),rgb(180,46,22),rgb(255,177,100),rgb(255,148,255)'; 
  SELECT @custom_value_id = custom_value_id FROM t_client_custom (NOLOCK)WHERE client_id = 666 AND custom_type_id = 4212;
  IF @custom_value_id IS NOT NULL 
   BEGIN 
    UPDATE t_custom_value SET char_value = @color_palette WHERE custom_value_id = @custom_value_id AND custom_type_id = 4212; 
   END 
  ELSE 
   BEGIN 
    DELETE t_client_custom where custom_type_id = 4212; 
    DELETE t_custom_value where custom_type_id = 4212 and default_value = 0; 
    INSERT[dbo].[t_custom_value](custom_type_id, description, char_value, default_value) 
    SELECT 4212, 'Custom Color Palette for Client', @color_palette, 0; 
    SELECT @custom_value_id = custom_value_id 
    FROM t_custom_value 
    WHERE custom_type_id = 4212 
    AND default_value = 0; 
    INSERT t_client_custom (client_id, custom_value_id, custom_type_id) 
    SELECT 666, @custom_value_id, 4212; 
   END 
END 
GO

--email domain
use tele_config_prod_db
insert into t_client_permission values (666, 20041)
insert into t_email_domain values ('one.com',0)
insert into t_email_domain values ('two.com',0)
insert into t_client_email_domain values (666, 1)

--backendapi user
insert into cv_system_db.dbo.t_backendapi_client_config values (666, 'teleapi','test1234', '')

--Remove language that is not setup in tele
use tele_config_prod_db
delete from t_client_language where language_id=2
--The statement data is returned using a new and refined version of sproc to have summarized data.
insert into t_client_permission values (666,268)
--paperless permission stmt list col on
insert into t_client_permission values (666,285)
--ensure all accts are displayed
update t_acct set display_flg=1 where master_flg=0
update t_subacct set display_flg=1 where display_flg=0

--reset cat user
update t_ce_csr_user set login_locked_date = NULL where ce_csr_user_id in (select ce_csr_user_id from t_ce_csr_user)
update t_ce_csr_user set login_attempts = NULL where ce_csr_user_id in (select ce_csr_user_id from t_ce_csr_user)
update t_ce_csr_user set last_login = DATEADD(DAY, -7, GETDATE()) where ce_csr_user_id in (select ce_csr_user_id from t_ce_csr_user)
update t_ce_csr_password_history set create_date = DATEADD(DAY, -7, GETDATE()) where user_id in (select ce_csr_user_id from t_ce_csr_user)
update [cv_system_db].[dbo].[t_ce_csr_internal_user] set login_locked_date = NULL where ce_csr_internal_user_id = (select ce_csr_internal_user_id from cv_system_db.dbo.t_ce_csr_internal_user)
update [cv_system_db].[dbo].[t_ce_csr_internal_user] set login_attempts = NULL where ce_csr_internal_user_id = (select ce_csr_internal_user_id from cv_system_db.dbo.t_ce_csr_internal_user)
update [cv_system_db].[dbo].[t_ce_csr_internal_user] set last_login = DATEADD(DAY, -7, GETDATE()) where ce_csr_internal_user_id = (select ce_csr_internal_user_id from cv_system_db.dbo.t_ce_csr_internal_user)
update [cv_system_db].[dbo].t_ce_csr_internal_password_history set create_date = DATEADD(DAY, -7, GETDATE()) where user_id = (select ce_csr_internal_user_id from cv_system_db.dbo.t_ce_csr_internal_user)


--create new john user
use tele_config_prod_db
DECLARE @user_id INT, @query_action TINYINT, @res INT, @AcctNum AS nvarchar(200), @assigned_acct_num AS nvarchar(200), @username as nvarchar(200), @lastname as nvarchar(200), @product_codes as nvarchar(10) ;
	SET @AcctNum			=	'5702016798'; --'SGD02V'--	'3NBV2T';				Master Acct / Organization Name  lots of accts
	SET @assigned_acct_num	=	'7700-077946'; --'287280707812_59428280' --'287280663200_59427704';		
	SET @username			=	'newjohn';
	SET @lastname			=	'Smith';
	SET @product_codes		=	'BA';
	
EXEC sp_backendapi_user_get
      @username				= @username,
      @client_id 			= 666,
      @create_user 			= 1,
      @user_id 				= @user_id OUTPUT,
      @query_action 		= @query_action OUTPUT
      
EXEC @res 					= sp_backendapi_user_update
      @user_id 				= @user_id,
      @email_addr 			= 'amak@globys.com',
      @email_type_id 		= 0,
      @pay_perm 			= 1,
      @payedit_perm 		= 1,
      @termsofuse 			= NULL,
      @firstname 			= 'Lots',
      @lastname 			= @lastname,
      @language_id 			= 0,
      @load_email_on 		= 1,
      @currency_code 		= 'USD',
      @permission_xml 		= NULL,
      @mobile_number 		= NULL

SELECT @res AS 'Result'

UPDATE t_user 
	SET password 			= '87F326B2351886D21ACAF357D22EFB5E' --test1234
WHERE user_id 				= @user_id 

EXEC sp_backendapi_hierarchy_user_add 
      @user_id 				= @user_id, 
      @client_id 			= 666, 
      @acct_num 			= @AcctNum, 
      @assigned_acct_num	= @assigned_acct_num, 
      @user_state 			= 5, 					-- Since we are in SSO Mode now, The @user_state = 1 should be 5 for this sproc call at the bottom
      @product_codes 		= @product_codes
GO

DECLARE @user_id INT
SET @user_id = (select user_id from t_user where login='newjohn')
insert into t_user_permission values (@user_id, 511, 1)
insert into t_user_permission values (@user_id, 63, 1)
insert into t_user_permission values (@user_id, 4, 1)
--insert into t_user_permission values (@user_id, 2, 1)
update t_user_permission set negative=1 where user_id=@user_id and permission_id=2
insert into t_user_permission values (@user_id, 55, 1)
insert into t_user_permission values (@user_id, 930, 1)
insert into t_user_report_type values (31, @user_id)

--setup single bill user
update t_user set password='87F326B2351886D21ACAF357D22EFB5E' where login='testuser1'
update t_acct set organization_id=19437 where acct_id=65760

--setup single bill user payment
DECLARE @singlebill INT
SET @singlebill = (select user_id from t_user where login='testuser1')
insert into t_user_permission values (@singlebill, 860, 0)
insert into t_user_permission values (@singlebill, 2, 0)

--Set excel threshold value
update t_custom_value set num_value=10000 where custom_type_id=3856
insert into t_client_custom values (666, 2514, 3856)

--update pagination
use tele_config_prod_db
update t_custom_value set char_value='5,10,50,100,250' where custom_type_id=2308

--update future payment date
update t_custom_value set char_value = '{''paymentMaxDaysInFuture'':10, ''useDueDate'':true, ''displayNavBar'': true, ''defaultProvider'': ''globyscc''}' where custom_type_id=10005

--timeout modal
insert into t_client_permission values (666, 1100)

--import hierarchy permission
use tele_config_prod_db
IF EXISTS(SELECT * FROM t_upgrade_history WHERE event = 'Core - Step2-StoredProcs (config_prod_db)' AND app_version like '27.2.0.%')
BEGIN
insert into t_product_permission_user_type values (1,1200,3)
insert into t_client_permission values (666,955)
update cv_system_db..t_hierarchy_import_client_config set import_processing_class = 'Process216' where client_id = 666
END

--account display
use tele_config_prod_db
insert into t_client_permission values (666,89)
DECLARE @userid INT = (select user_id from t_user where login='jbrennan')
insert into t_user_permission values (@userid, 89,0)

--monthly spend widget 16 months
update t_custom_value set num_value=16 where custom_type_id=20401
