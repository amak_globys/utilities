numOrgs = 2
fanStart = 9502651
banStart = 44790572
numFans = 500
numBans = 1000
newOrgName='SS1I397'
f = open(str(numOrgs) + "_" + str(numFans) + "_" + str(numBans) + "_acctdetail.dat", "w")

for orgs in range(1, numOrgs + 1):
    org_name=newOrgName + str(orgs)
    f.write('289|' + org_name + '|Mark Methvin Homes ' + org_name + '|' + org_name + '|' + org_name + '|1|' + org_name + '|PremierLink|||||1|||' + org_name + '|Agreement||0|USD||' + org_name + '|\n')
    for fans in range(1, numFans + 1):
        f.write('289|' + str(fanStart) + '|MARK METHVIN HOMES, INC-N CBE CRU ' + str(fanStart) + '|' + org_name + '|' + org_name + '|0|' + str(fanStart) + '|PremierLink|||||1|7,5||' + str(fanStart) + '|FAN||1|USD|ATT|' + str(fanStart) + '|\n')
        #289|04502656|MARK METHVIN HOMES, INC-N CBE CRU|S1I397|S1I397|0|04502656|PremierLink|||||1|7,5||04502656|FAN||1|USD|ATT|04502656|
        for bans in range(1, numBans + 1):
            f.write('289|' + str(banStart) + '|MARK METHVIN HOMES, INC ' + str(banStart) + '|' + org_name + '|' + org_name + '|0|' + str(banStart) + '|PremierLink|||||1|3,6,9,5||' + str(banStart) + '|BAN||1|USD|ATT|' + str(fanStart) + '|\n')
            banStart = banStart + 1
            #289|827905722|MARK METHVIN HOMES, INC|S1I397|S1I397|0|827905722|PremierLink|||||1|3,6,9,5||827905722|BAN||1|USD|ATT|04502656|
        fanStart = fanStart + 1
f.close()
