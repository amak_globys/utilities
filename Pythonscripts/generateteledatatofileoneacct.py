import random
from datetime import datetime
from dateutil.relativedelta import relativedelta

# pip install python-dateutil if you don't have this module
def fun ():
    num_list = ["112018IOT"]    
    f = open("16months_statement.dat", "w")
    for accts in num_list:
        n = 1
        current_date = datetime.today()
        past_date = current_date - relativedelta(months=n)
        new_date = past_date

        for x in range(0, n):
            day = random.randrange(1, 28, 1)
            invoice = random.randrange(10000000, 99999999, 1)
            previous_balance = round(random.uniform(20,50), 2)
            other_charges_credits = round(random.uniform(1,5), 2)
            datem = new_date.strftime('%b')
            datey = new_date.strftime('%y')
            startdate = str(day) + "-" + str(datem) + "-" + str(datey)
            day = random.randrange(1, 28, 1)
            billdate = new_date + relativedelta(months=1)
            datem = billdate.strftime('%b')
            enddate = str(day) + "-" + str(datem) + "-" + str(datey)
            f.write ("666|{}|{}|1|{}|{}|0|Account Activity (incl GST)|10|Previous Balance|10|||||||||||||||||{}||0|0|0|0|||||||||\n".format(accts, startdate, invoice, enddate, previous_balance))
            f.write ("666|{}|{}|1|{}|{}|0|Account Activity (incl GST)|10|Payments Received|20|||||||||||||||||-{}||0|0|0|0|||||||||\n".format(accts, startdate, invoice, enddate, previous_balance))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Other Charges and Credits|20|Account|10|Rising Fawn, NC||||||||||||||{}||0|0|0|0|50||||3|OCCACCT|||\n".format(accts, startdate, invoice, enddate,other_charges_credits))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Call and Usage|10|National Calls|150|Beaver Dam, WV||||||||||||||{}||0|0|0|0|50||||3|CNATC|||\n".format(accts, startdate, invoice, enddate, round(random.uniform(0,20), 2)))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Call and Usage|10|Other Usage|180|Conowingo, GA||||||||||||||{}||0|0|0|0|50||||3|CUOU|||\n".format(accts, startdate, invoice, enddate, round(random.uniform(0,20), 2)))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Call and Usage|10|Call Forwarded|10|Mesick, OK||||||||||||||{}||0|0|0|0|50||||3|CUCFWD|||\n".format(accts, startdate, invoice, enddate, round(random.uniform(0,20), 2)))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Call and Usage|10|MessageBank|120|Aiea, TN||||||||||||||{}||0|0|0|0|50||||3|CMSGBNK|||\n".format(accts, startdate, invoice, enddate, round(random.uniform(0,20), 2)))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Call and Usage|10|Other Calls|160|Mililani, NC||||||||||||||{}||0|0|0|0|50||||3|CUOC|||\n".format(accts, startdate, invoice, enddate, round(random.uniform(0,20), 2)))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Service and Equipment|30|Mobile|40|Egorkovskoi, NF||||||||||||||24.99||0|0|0|0|50||||3|SEMOB|||\n".format(accts, startdate, invoice, enddate, round(random.uniform(0,20), 2)))
            f.write ("666|{}|{}|1|{}|{}|2|GST - click to view|30|||||||||||||||||||0.00||0|0|0|0|38||||1|GST|1||\n".format(accts, startdate, invoice, enddate))
            new_date = new_date + relativedelta(months=1)
        
    f.close()
fun()


"""
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|0|Account Activity (incl GST)|10|Previous Balance|10|||||||||||||||||53.93||0|0|0|0|||||||||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|0|Account Activity (incl GST)|10|Payments Received|20|||||||||||||||||-53.93||0|0|0|0|||||||||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Other Charges and Credits|20|Account|10|Rising Fawn, NC||||||||||||||0.59||0|0|0|0|50||||3|OCCACCT|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Call and Usage|10|National Calls|150|Beaver Dam, WV||||||||||||||11.55||0|0|0|0|50||||3|CNATC|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Call and Usage|10|Other Usage|180|Conowingo, GA||||||||||||||0.26||0|0|0|0|50||||3|CUOU|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Call and Usage|10|Call Forwarded|10|Mesick, OK||||||||||||||0.00||0|0|0|0|50||||3|CUCFWD|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Call and Usage|10|MessageBank|120|Aiea, TN||||||||||||||0.00||0|0|0|0|50||||3|CMSGBNK|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Call and Usage|10|Other Calls|160|Mililani, NC||||||||||||||-10.00||0|0|0|0|50||||3|CUOC|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Service and Equipment|30|Mobile|40|Egorkovskoi, NF||||||||||||||24.99||0|0|0|0|50||||3|SEMOB|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|2|GST - click to view|30|||||||||||||||||||0.00||0|0|0|0|38||||1|GST|1||
"""
