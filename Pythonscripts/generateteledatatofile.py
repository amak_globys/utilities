import random
from datetime import datetime
from dateutil.relativedelta import relativedelta

# pip install python-dateutil if you don't have this module
def fun ():
"""
    num_list = [
        "0096737060-1248692744-G", "0164005894-1248692744-G", "0225466330-1248692744-G", "0225466966-1248692744-G", "0234899120-1248692744-G",
        "0234899349-1248692744-G", "0234899838-1248692744-G", "0234899871-1248692744-G", "0402051817-1248692744-G", "0499144745-1248692744-G",
        "0531530557-1248692744-G", "0531530776-1248692744-G", "0531530792-1248692744-G", "0593685020-1248692744-G", "0594728379-1248692744-G",
        "0594728961-1248692744-G", "0732411948-1248692744-G", "0732412603-1248692744-G", "0732412725-1248692744-G", "0732414032-1248692744-G",
        "0732414550-1248692744-G", "0732415148-1248692744-G", "0732415189-1248692744-G", "0732415357-1248692744-G", "0732415669-1248692744-G",
        "0732415780-1248692744-G", "0732415922-1248692744-G", "0732490621-1248692744-G", "0732490700-1248692744-G", "0732493088-1248692744-G",
        "0732493726-1248692744-G", "0732493762-1248692744-G", "0732493967-1248692744-G", "0732495227-1248692744-G", "0732495280-1248692744-G",
        "0732495519-1248692744-G", "0732495647-1248692744-G", "0732495659-1248692744-G", "0732495696-1248692744-G", "0732495721-1248692744-G",
        "0732495758-1248692744-G", "0732496630-1248692744-G", "0732496681-1248692744-G", "0732498319-1248692744-G", "0732499150-1248692744-G",
        "0732499220-1248692744-G", "0732499749-1248692744-G", "0736430108-1248692744-G", "0906202852-1248692744-G", "0949398451-1248692744-G",
        "4155460530-1248692744-G", "4560169343-1248692744-G", "4627144527-1248692744-G", "4633370763-1248692744-G", "4804073452-1248692744-G",
        "4887167765-1248692744-G", "4919090419-1248692744-G", "4936924500-1248692744-G", "6170838401-1248692744-W", "6337483401-1248692744-W",
        "7483480222-1248692744-G", "7492894443-1248692744-G", "7679652585-1248692744-G", "7733556829-1248692744-G", "7741112256-1248692744-G",
        "7773653386-1248692744-G", "7777742902-1248692744-G", "7780327614-1248692744-G", "7783734652-1248692744-G", "7783734686-1248692744-G",
        "7783740025-1248692744-G", "7783740120-1248692744-G", "7783740139-1248692744-G", "7783740166-1248692744-G", "7783740212-1248692744-G",
        "7783740423-1248692744-G", "7783740478-1248692744-G", "7783740552-1248692744-G", "7783740569-1248692744-G", "7783740586-1248692744-G",
        "7783740817-1248692744-G", "7783743426-1248692744-G", "7783743548-1248692744-G", "7783743550-1248692744-G", "7783747167-1248692744-G",
        "7783747361-1248692744-G", "7783747482-1248692744-G", "7783747506-1248692744-G", "7783747521-1248692744-G", "7783747587-1248692744-G",
        "7783747772-1248692744-G", "7783747922-1248692744-G", "7783747998-1248692744-G", "7783748402-1248692744-G", "7783748431-1248692744-G",
        "7783748488-1248692744-G", "7783748621-1248692744-G", "7783749598-1248692744-G", "7960118136-1248692744-G", "9123054788-1248692744-G"]
"""

    num_list = ["112018IOT"]    
    f = open("16months_statement.dat", "w")
    for accts in num_list:
        n = 1
        current_date = datetime.today()
        past_date = current_date - relativedelta(months=n)
        new_date = past_date

        for x in range(0, n):
            day = random.randrange(1, 28, 1)
            invoice = random.randrange(10000000, 99999999, 1)
            previous_balance = round(random.uniform(20,50), 2)
            other_charges_credits = round(random.uniform(1,5), 2)
            datem = new_date.strftime('%b')
            datey = new_date.strftime('%y')
            startdate = str(day) + "-" + str(datem) + "-" + str(datey)
            day = random.randrange(1, 28, 1)
            billdate = new_date + relativedelta(months=1)
            datem = billdate.strftime('%b')
            enddate = str(day) + "-" + str(datem) + "-" + str(datey)
            f.write ("666|{}|{}|1|{}|{}|0|Account Activity (incl GST)|10|Previous Balance|10|||||||||||||||||{}||0|0|0|0|||||||||\n".format(accts, startdate, invoice, enddate, previous_balance))
            f.write ("666|{}|{}|1|{}|{}|0|Account Activity (incl GST)|10|Payments Received|20|||||||||||||||||-{}||0|0|0|0|||||||||\n".format(accts, startdate, invoice, enddate, previous_balance))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Other Charges and Credits|20|Account|10|Rising Fawn, NC||||||||||||||{}||0|0|0|0|50||||3|OCCACCT|||\n".format(accts, startdate, invoice, enddate,other_charges_credits))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Call and Usage|10|National Calls|150|Beaver Dam, WV||||||||||||||{}||0|0|0|0|50||||3|CNATC|||\n".format(accts, startdate, invoice, enddate, round(random.uniform(0,20), 2)))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Call and Usage|10|Other Usage|180|Conowingo, GA||||||||||||||{}||0|0|0|0|50||||3|CUOU|||\n".format(accts, startdate, invoice, enddate, round(random.uniform(0,20), 2)))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Call and Usage|10|Call Forwarded|10|Mesick, OK||||||||||||||{}||0|0|0|0|50||||3|CUCFWD|||\n".format(accts, startdate, invoice, enddate, round(random.uniform(0,20), 2)))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Call and Usage|10|MessageBank|120|Aiea, TN||||||||||||||{}||0|0|0|0|50||||3|CMSGBNK|||\n".format(accts, startdate, invoice, enddate, round(random.uniform(0,20), 2)))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Call and Usage|10|Other Calls|160|Mililani, NC||||||||||||||{}||0|0|0|0|50||||3|CUOC|||\n".format(accts, startdate, invoice, enddate, round(random.uniform(0,20), 2)))
            f.write ("666|{}|{}|1|{}|{}|1|New Charges (incl GST)|20|Service and Equipment|30|Mobile|40|Egorkovskoi, NF||||||||||||||24.99||0|0|0|0|50||||3|SEMOB|||\n".format(accts, startdate, invoice, enddate, round(random.uniform(0,20), 2)))
            f.write ("666|{}|{}|1|{}|{}|2|GST - click to view|30|||||||||||||||||||0.00||0|0|0|0|38||||1|GST|1||\n".format(accts, startdate, invoice, enddate))
            new_date = new_date + relativedelta(months=1)
        
    f.close()
fun()


"""
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|0|Account Activity (incl GST)|10|Previous Balance|10|||||||||||||||||53.93||0|0|0|0|||||||||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|0|Account Activity (incl GST)|10|Payments Received|20|||||||||||||||||-53.93||0|0|0|0|||||||||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Other Charges and Credits|20|Account|10|Rising Fawn, NC||||||||||||||0.59||0|0|0|0|50||||3|OCCACCT|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Call and Usage|10|National Calls|150|Beaver Dam, WV||||||||||||||11.55||0|0|0|0|50||||3|CNATC|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Call and Usage|10|Other Usage|180|Conowingo, GA||||||||||||||0.26||0|0|0|0|50||||3|CUOU|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Call and Usage|10|Call Forwarded|10|Mesick, OK||||||||||||||0.00||0|0|0|0|50||||3|CUCFWD|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Call and Usage|10|MessageBank|120|Aiea, TN||||||||||||||0.00||0|0|0|0|50||||3|CMSGBNK|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Call and Usage|10|Other Calls|160|Mililani, NC||||||||||||||-10.00||0|0|0|0|50||||3|CUOC|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|1|New Charges (incl GST)|20|Service and Equipment|30|Mobile|40|Egorkovskoi, NF||||||||||||||24.99||0|0|0|0|50||||3|SEMOB|||
666|7780327614-1248692744-G|16-Sep-15|1|45626951|04-Oct-15|2|GST - click to view|30|||||||||||||||||||0.00||0|0|0|0|38||||1|GST|1||
"""
